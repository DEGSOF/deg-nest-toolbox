import { GenericDto } from './generic.dto';

describe('GenericDto', () => {
  // Should allow instantiation without any arguments
  it('should allow instantiation without any arguments', () => {
    const genericDto = new GenericDto();
    expect(genericDto).toBeInstanceOf(GenericDto);
  });
});
