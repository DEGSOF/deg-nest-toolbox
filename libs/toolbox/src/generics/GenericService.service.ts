import { instanceToPlain } from 'class-transformer';
import { BaseEntity, FindManyOptions, FindOneOptions, In, LessThan, MoreThan, RemoveOptions, SaveOptions } from 'typeorm';

import { FindOffsetPaginationResponse } from './../generics/types/find-offset-pagination-response-type';
import { FindRequest } from './../generics/types/find-request.type';
import { GenericRepository } from './GenericRepository.repository';
import { FindCursorResponse } from './types/find-response.type';
import { PageInfo } from './types/page-info.type';

export class GenericService<_Entity extends BaseEntity & { id?: any }> {
  constructor(private readonly genericRepository: GenericRepository<_Entity>) {}

  /**
   * @function: findOffsetPagination - This method will return a offset based pagination response
   * @description: This method will return a offset based pagination response
   * @param options: FindManyOptions - options to be applied to the query from typeorm
   * @returns Promise<FindCursorResponse>
   */

  async findOffsetPagination(options: FindManyOptions): Promise<FindOffsetPaginationResponse> {
    const result = await this.genericRepository.find(options);
    const count = await this.genericRepository.count(options);
    const total = await this.genericRepository.count();
    //get the total number of records and find the next offset value and previous offset value
    const nextOffset = Number(options.skip) + Number(options.take);
    const previousOffset = Number(options.skip) - Number(options.take) < 0 ? 0 : Number(options.skip) - Number(options.take);
    return {
      records: result,
      count: count,
      total: total,
      nextOffset: nextOffset,
      previousOffset: previousOffset,
    };
  }

  /**
   * @function: executeRawQuery - This method will return raw query result
   * @param query: string - query to be executed
   * @param parameters: any[] - parameters to be applied to the query
   * @returns Promise<FindCursorResponse>
   */

  executeRawQuery(query: string, parameters?: any[]): Promise<any> {
    return this.genericRepository.executeRawQuery(query, parameters);
  }

  /**
   * @function: saveRecord - This method will save a record in the database
   * @param entity: _Entity | _Entity[] must extend from BaseEntity and have an id property from typeorm
   * @param options: SaveOptions - options to be applied to the query from typeorm
   * @returns a single entity or an array of entities
   */

  async saveRecord(entity: _Entity | _Entity[], options?: SaveOptions): Promise<_Entity | _Entity[]> {
    return await this.genericRepository.saveRecord(entity, options);
  }

  /**
   * @function: deleteRecord - This method will delete a record from the database (hard delete)
   * @param criteria: FindOneOptions<_Entity> - options to be applied to the query from typeorm
   * @param options: RemoveOptions - options to be applied to the query from typeorm
   * @returns: a single entity or an array of entities
   */

  async deleteRecord(criteria: FindOneOptions<_Entity>, options?: RemoveOptions): Promise<_Entity | _Entity[]> {
    return await this.genericRepository.deleteRecord(criteria, options);
  }

  /**
   * @function: softDeleteRecord - This method will soft delete a record from the database
   * @param criteria: FindOneOptions<_Entity> - options to be applied to the query from typeorm
   * @param options: SaveOptions - options to be applied to the query from typeorm
   * @returns: a single entity or an array of entities
   */

  async softDeleteRecord(criteria: FindOneOptions<_Entity>, options?: SaveOptions): Promise<_Entity | _Entity[]> {
    return await this.genericRepository.softDeleteRecord(criteria, options);
  }

  /**
   * @function: findCursorPagination - This method will return a cursor based pagination response
   * @description: This method will return a cursor based pagination response
   * @param request: FindRequest
   * @param mustInvert: boolean - if true, the order will be inverted
   * @param mappedFilters: Array<object> - filters to be applied to the query (where clause) - example: [{name: 'John'}, {age: 20}]
   * @param mappedSearch: Array<object> - search to be applied to the query (where clause) - example: [{name: 'John'}, {age: 20}]
   * @param relations: string[] - relations to be loaded
   * @returns Promise<FindCursorResponse>
   */

  async findCursorPagination(request?: FindRequest, mustInvert = false, mappedFilters: Array<object> = [], mappedSearch: Array<object> = [], relations: string[] = []): Promise<FindCursorResponse> {
    console.log('mustInvert', mustInvert);
    let where = [];
    let inverseOrder = {};
    let regularOrder = {};
    let limit = 30;
    let reverted = false;
    let onlyCountCondition = [];
    if (request) {
      const { first, last, after, before } = request;
      if (first && last) {
        throw new Error('first and last cannot be used together');
      }
      if (after && before) {
        throw new Error('after and before cannot be used together');
      }
      if (first) {
        limit = first;
      }
      if (last) {
        limit = last;
      }
      if (!after && !before && mustInvert) {
        regularOrder = { id: 'ASC' };
        inverseOrder = { id: 'DESC' };
      }
      if (after) {
        where.push({ id: MoreThan(after) });
        regularOrder = { id: 'DESC' };
        inverseOrder = { id: 'ASC' };
        if (mustInvert) {
          regularOrder = { id: 'DESC' };
          inverseOrder = { id: 'DESC' };
          where = [];
          where.push({ id: LessThan(after) });
        }
      }
      if (before) {
        where.push({ id: LessThan(before) });
        regularOrder = { id: 'ASC' };
        inverseOrder = { id: 'DESC' };
        if (mustInvert) {
          reverted = true;
          regularOrder = { id: 'DESC' };
          inverseOrder = { id: 'ASC' };
          where = [];
          where.push({ id: MoreThan(before) });
        }
      }
      if (last) {
        limit = last;
      }
      if (first) {
        limit = first;
      }
    }

    let filters: any = [];
    if (mappedFilters.length > 0) {
      filters = Object.assign({}, ...mappedFilters);
      where[0] = { ...where[0], ...filters };
      onlyCountCondition.push(filters);
    }

    if (mappedSearch.length > 0) {
      const search = mappedSearch;
      if (Object.keys(search).length > 0) {
        where = [];
        where.push(search);
        onlyCountCondition = [];
        onlyCountCondition.push(search);
      }
    }
    if (where.length === 0) {
      where.push(filters);
    }
    where = where.filter((element) => element !== undefined && element !== null && Object.keys(element).length > 0);
    onlyCountCondition = onlyCountCondition.filter((element) => element !== undefined && element !== null && Object.keys(element).length > 0);
    if (where.length === 0) {
      where = undefined;
    }
    if (onlyCountCondition.length === 0) {
      onlyCountCondition = undefined;
    }
    let ids: any = await this.genericRepository.find({
      select: {
        id: true,
      } as any,
      where: where,
      order: regularOrder,
    });
    if (ids.length > 0) {
      ids = ids.map((item) => item.id);
    }
    limit = limit > 0 && limit < 31 ? limit : 30;
    let edges = await this.genericRepository.find({
      where: {
        id: In(ids) as any,
      },
      order: inverseOrder,
      relations: relations.length > 0 ? relations : undefined,
      loadEagerRelations: true,
      take: limit,
    });
    const totalCount = await this.genericRepository.count({
      where: onlyCountCondition,
      order: inverseOrder,
      relations: relations.length > 0 ? relations : undefined,
      loadEagerRelations: true,
      take: limit,
    });
    const pages = Math.ceil(totalCount / limit);
    if (reverted === true && edges.length > 0) {
      edges = edges.reverse();
    }
    const pageInfo: PageInfo = new PageInfo();
    pageInfo.endCursor = edges.length > 0 ? edges[0]['id'] : null;
    pageInfo.startCursor = edges.length > 0 ? edges[edges.length - 1]['id'] : null;
    pageInfo.hasNextPage = pages > 1;
    pageInfo.hasPreviousPage = pages + 1 < pages;
    const formatedEdges =
      edges.length > 0
        ? edges.map((edge: any) => {
            return {
              cursor: edge.id,
              node: instanceToPlain(edge, { enableCircularCheck: true }),
            };
          })
        : [];
    return {
      edges: formatedEdges,
      pageInfo: pageInfo,
      totalCount: totalCount,
    };
  }

  /**
   * @function: findOne - This method will return a single entity
   * @param criteria: FindOneOptions<_Entity> - options to be applied to the query from typeorm
   * @returns Promise<_Entity>
   */

  async findOne(criteria: FindOneOptions<_Entity>): Promise<_Entity> {
    return (await this.genericRepository.findOne(criteria)) ?? null;
  }

  /**
   * @function: find - This method will return an array of entities
   * @param criteria: FindManyOptions<_Entity> - options to be applied to the query from typeorm
   * @returns Promise<_Entity[]>
   */

  async find(criteria: FindManyOptions<_Entity>): Promise<_Entity[]> {
    return await this.genericRepository.find(criteria);
  }

  getRepository(): GenericRepository<_Entity> {
    return this.genericRepository;
  }
}
