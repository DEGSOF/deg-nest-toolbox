import { injectUserTo, injectUserToBody, injectUserToParam, injectUserToQuery } from './inject.user.decorator';

describe('User Decorator', () => {
  it('should inject the user in the request body', () => {
    const injected = injectUserTo('body');
    expect(injected).toBeTruthy();
  });
  it('should inject the user in the request params', () => {
    const injected = injectUserToBody();
    expect(injected).toBeTruthy();
  });
  it('should inject the user in the request query', () => {
    const injected = injectUserToParam();
    expect(injected).toBeTruthy();
  });
  it('should inject the user in the request params', () => {
    const injected = injectUserToQuery();
    expect(injected).toBeTruthy();
  });
});
