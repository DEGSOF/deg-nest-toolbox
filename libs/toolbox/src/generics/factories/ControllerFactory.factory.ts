import { BadRequestException, Body, Delete, Get, NotFoundException, Param, Post, Put, Query, Type, UsePipes } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiSecurity } from '@nestjs/swagger';
import { plainToInstance } from 'class-transformer';
import { BaseEntity } from 'typeorm';

import { GenericService } from '../GenericService.service';
import { ICrudController } from '../interfaces/ICrudController.interface';
import { AbstractValidationPipe } from '../pipes/AbstractValidationPipe.pipe';
import { FindOffsetPaginationResponse } from './../../generics/types/find-offset-pagination-response-type';

export function controllerFactory<T extends BaseEntity & { id?: any }, C, U>(entity: Type<T | Array<T>>, createDto: Type<C>, updateDto: Type<U>): Type<ICrudController<T, C, U>> {
  const createPipe = new AbstractValidationPipe({ whitelist: true, transform: true }, { body: createDto });
  const updatePipe = new AbstractValidationPipe({ whitelist: true, transform: true }, { body: updateDto });

  /**
   * @class: CrudController
   * @description: This class will be used to create a controller for REST API
   * @param entity: Type<T | Array<T>> - entity type
   * @param createDto: Type<C> - create dto type
   * @param updateDto: Type<U> - update dto type
   * @returns: Type<ICrudController<T, C, U>>
   */

  class CrudController<T extends BaseEntity, C, U> implements ICrudController<T, C, U> {
    public service: GenericService<T>;

    constructor(service: GenericService<T>) {
      this.service = service;
    }

    @Post()
    @ApiSecurity('bearer')
    @ApiOperation({ description: 'Create a new entity' })
    @ApiBody({
      type: createDto,
      required: true,
      description: 'Create a new entity',
      schema: { example: new createDto() },
    })
    @ApiResponse({
      status: 201,
      description: 'Create 1 record of entity type',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              data: {
                type: 'array',
              },
              statusCode: {
                type: 'number',
              },
            },
            example: {
              data: 'New ' + entity.name + ' record',
              statusCode: 201,
            },
          },
        },
      },
    })
    @ApiResponse({
      status: 400,
      description: 'Bad request',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              data: {
                type: 'array',
              },
              statusCode: {
                type: 'number',
              },
            },
            example: {
              data: 'Bad request',
              statusCode: 400,
            },
          },
        },
      },
    })
    @UsePipes(createPipe)
    async create(@Body() body: C): Promise<T | Array<T>> {
      const ent = (await plainToInstance(entity, body)) as unknown as T;
      return await this.service.saveRecord(ent);
    }

    @Put(':id')
    @ApiSecurity('bearer')
    @ApiOperation({ description: 'Update an entity' })
    @ApiBody({
      type: createDto,
      required: true,
      description: 'Update an entity',
      schema: { example: new updateDto() },
    })
    @ApiResponse({
      status: 200,
      description: 'Update 1 record of entity type',
      content: {
        'application/json': {
          schema: {
            type: updateDto.name,
            example: {
              data: 'Entity ' + entity.name + ' updated',
              statusCode: 200,
            },
          },
        },
      },
    })
    @ApiResponse({
      status: 400,
      description: 'Bad request',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              data: {
                type: 'array',
              },
              statusCode: {
                type: 'number',
              },
            },
            example: {
              data: 'Bad request',
              statusCode: 400,
            },
          },
        },
      },
    })
    @UsePipes(updatePipe)
    async update(@Param('id') id: number | string, @Body() body: U): Promise<T | Array<T>> {
      let where: object = {};
      where = {
        where: { id: id },
      };
      let entity = await this.service.findOne(where);
      if (!entity) {
        throw new NotFoundException('Element not found');
      }
      entity = Object.assign(entity, body);
      return await this.service.saveRecord(entity);
    }

    @ApiSecurity('bearer')
    @ApiOperation({ description: 'Get a entity' })
    @ApiBody({
      type: Promise<T>,
      required: true,
      description: 'Get a entity',
      schema: { example: { name: 'test' } },
    })
    @ApiResponse({
      status: 200,
      description: 'Content of entity',
    })
    @ApiResponse({
      status: 404,
      description: 'Element not found',
    })
    @Get(':id')
    async getOne(@Param('id') id: number | string): Promise<T> {
      let where: object = {};
      where = {
        where: { id: id },
      };
      const entity = await this.service.findOne(where);
      if (!entity) {
        throw new NotFoundException('Element not found');
      }
      return entity;
    }

    @Get()
    @ApiSecurity('bearer')
    @ApiOperation({
      description: 'Get all entities',
      responses: { '200': { description: 'Array record' } },
      parameters: [],
    })
    @ApiResponse({
      status: 200,
      description: 'Get all entities',
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties: {
              data: {
                type: 'array',
              },
              statusCode: {
                type: 'number',
              },
            },
            example: {
              data: [],
              statusCode: 200,
            },
          },
        },
      },
    })
    async findByOffsetPaginationResponse(@Query() query: T & { take: number; offset: number }): Promise<FindOffsetPaginationResponse> {
      let where = [];
      //remove take and offset from query object and store it in a variable, for the rest of the query object, we will use it as a where clause
      const { take, offset, ...rest } = query;
      //loop through the rest of the query object and push it to the where clause
      Object.keys(rest).forEach((key) => {
        where.push({ [key]: rest[key] });
      });
      //if where contains a value that was not in the entity type T, we will throw an error
      if (where.length > 0) {
        const keys = Object.keys(where[0]);
        keys.forEach((key) => {
          if (!(key in entity)) {
            //we include the key in the error message
            throw new BadRequestException('Invalid query parameter: ' + key);
          }
        });
      }
      //if there is no where clause, we will set it to empty object
      if (where.length === 0) {
        where = undefined;
      }
      const entities = await this.service.findOffsetPagination({
        take: take ?? 30,
        skip: offset ?? 0,
        where: where,
      });
      if (!entities) {
        return {
          records: [],
          count: 0,
          total: 0,
          nextOffset: 0,
          previousOffset: 0,
        };
      }
      return entities;
    }

    @ApiSecurity('bearer')
    @ApiOperation({ description: 'Delete entity' })
    @ApiBody({
      type: createDto,
      required: true,
      description: 'Delete entity',
      schema: { example: new entity() },
    })
    @ApiResponse({
      status: 200,
      description: 'Content deleted',
      schema: { example: new entity() },
    })
    @ApiResponse({
      status: 404,
      description: 'Element not found',
      schema: { example: { message: 'Element not found' } },
    })
    @Delete(':id')
    async delete(@Param() id: number | string): Promise<T | T[]> {
      let where: object = {};
      where = {
        where: { id: id },
      };
      return await this.service.softDeleteRecord(where);
    }
  }

  return CrudController as any;
}
