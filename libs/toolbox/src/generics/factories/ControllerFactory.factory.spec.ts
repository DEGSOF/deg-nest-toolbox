import { Type } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { BaseEntity } from 'typeorm';

import { GenericService } from '../GenericService.service';
import { controllerFactory } from './ControllerFactory.factory';

describe('controllerFactory', () => {
  let module: TestingModule;
  let service: GenericService<BaseEntity>;
  let createDto: Type<any>;
  let updateDto: Type<any>;
  let entity: Type<any>;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      providers: [
        {
          provide: 'GenericService',
          useValue: {
            findOne: jest.fn(),
            findOffsetPagination: jest.fn(),
            saveRecord: jest.fn(),
            softDeleteRecord: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<GenericService<BaseEntity>>('GenericService');
    createDto = class {};
    updateDto = class {};
    entity = class {};
  });

  describe('controllerFactory', () => {
    it('should return a class', () => {
      const controller = controllerFactory(entity, createDto, updateDto);
      expect(controller).toBeDefined();
      expect(typeof controller).toBe('function');
    });
  });
});
