import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import * as moment from 'moment';

@ValidatorConstraint({ async: true })
class IsDateInFormatConstraint implements ValidatorConstraintInterface {
  async validate(value: any | Array<any>, args: ValidationArguments): Promise<boolean> {
    const [format] = args.constraints;
    return moment(value, format, true).isValid();
  }

  defaultMessage(args: ValidationArguments): string {
    const property = args.property;
    const [format] = args.constraints;
    return `${property} debe estar en el formato: ${format}.`;
  }
}

export function isDateInFormat(format: string, validationOptions?: ValidationOptions): (object: Record<string, any>, propertyName: string) => void {
  return function (object: Record<string, any>, propertyName: string): void {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [format],
      validator: IsDateInFormatConstraint,
    });
  };
}
