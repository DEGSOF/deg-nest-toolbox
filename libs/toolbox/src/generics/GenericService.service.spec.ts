import { GenericEntityGenerated } from './entities/GenericEntityGenerated';
import { GenericService } from './GenericService.service';

describe('GenericService', () => {
  let service: GenericService<any>;
  const repository = {
    find: jest.fn().mockResolvedValue([
      {
        id: 'qewewqewq1',
      },
      {
        id: 'qewewqewq2',
      },
      {
        id: 'qewewqewq3',
      },
      {
        id: 'qewewqewq4',
      },
    ]),
    findOne: jest.fn().mockResolvedValue(null),
    executeRawQuery: jest.fn().mockResolvedValue({}),
    saveRecord: jest.fn().mockResolvedValue({}),
    softDeleteRecord: jest.fn().mockResolvedValue({}),
    deleteRecord: jest.fn().mockResolvedValue({}),
    getRepository: jest.fn().mockResolvedValue({}),
    count: jest.fn().mockResolvedValue({}),
  } as any;

  beforeEach(async () => {
    service = new GenericService<any>(repository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should test the method findOffsetPagination', () => {
    const result = service.findOffsetPagination({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method find', () => {
    const result = service.find({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method findOne', () => {
    const result = service.findOne({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method executeRawQuery', () => {
    const result = service.executeRawQuery('');
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method saveRecord', () => {
    const result = service.saveRecord({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method softDeleteRecord', () => {
    const result = service.softDeleteRecord({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method deleteRecord', () => {
    const result = service.deleteRecord({});
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  it('should test the method getRepository', () => {
    const result = service.getRepository();
    expect(result).toBeDefined();
    expect(result).resolves;
  });

  // Should return a valid FindCursorResponse object when called with valid parameters
  it('should return a valid FindCursorResponse object when called with valid parameters', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      first: 10,
      after: '',
      filters: [],
    } as any;
    const mustInvert = true;
    const mappedFilters: Array<object> = [{ age: 20 }];
    const mappedSearch: Array<object> = [{ name: 'John' }];
    const relations: string[] = ['relation1', 'relation2'];
    // Act
    const result = await genericService.findCursorPagination(request, mustInvert, mappedFilters, mappedSearch, relations);
    // Assert
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });

  it('should return a valid FindCursorResponse object when called with valid parameters', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      first: 10,
      after: '',
      filters: [],
    } as any;
    // Act
    const result = await genericService.findCursorPagination(request);
    // Assert
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });

  it('should return a valid FindCursorResponse object when called with valid parameters', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      last: 10,
      filters: [],
    } as any;
    // Act
    const result = await genericService.findCursorPagination(request);
    // Assert
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });

  it('should throw and error if first and last are used together', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      first: 10,
      last: 10,
      filters: [],
    } as any;
    expect(genericService.findCursorPagination(request)).rejects.toThrow();
  });

  it('should throw and error if first and last are used together', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      after: 'qweqwe',
      before: 'qweqwe',
      filters: [],
    } as any;
    expect(genericService.findCursorPagination(request)).rejects.toThrow();
  });

  it('should throw and error if first and last are used together', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      after: 'qweqwe',
      filters: [],
    } as any;
    expect(genericService.findCursorPagination(request)).resolves;
  });

  it('should throw and error if first and last are used together', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {
      before: 'qweqwe',
      filters: [],
    } as any;
    expect(genericService.findCursorPagination(request)).resolves;
  });

  it('case', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const request = {} as any;
    const result = await genericService.findCursorPagination(request);
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });

  it('case', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const result = await genericService.findCursorPagination(
      {
        after: 'qweqweqweqweqwe',
      },
      true,
      [],
      [],
      [],
    );
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });

  it('case', async () => {
    // Arrange
    const genericService = new GenericService<GenericEntityGenerated>(repository);
    const result = await genericService.findCursorPagination(
      {
        before: 'qweqweqweqweqwe',
      },
      true,
      [],
      [],
      [],
    );
    expect(result).toBeDefined();
    expect(result.edges).toBeDefined();
    expect(result.pageInfo).toBeDefined();
    expect(result.totalCount).toBeDefined();
  });
});
