import { BaseEntity } from 'typeorm';

import { FindOffsetPaginationResponse } from '../types/find-offset-pagination-response-type';

export type ICrudController<EntityType extends BaseEntity & { id?: any }, CreateDto, UpdateDto> = {
  getOne(id: number | string): Promise<EntityType>;
  create(body: CreateDto): Promise<EntityType | Array<EntityType>>;
  update(id: number | string, body: UpdateDto): Promise<EntityType | Array<EntityType>>;
  delete(id: number | string): Promise<EntityType | EntityType[]>;
  findByOffsetPaginationResponse(query: EntityType): Promise<FindOffsetPaginationResponse>;
};
