import { CreateDateColumn, DeleteDateColumn, Timestamp, UpdateDateColumn } from 'typeorm';

import { EntityDefaultFunctions } from './GenericEntityDefaultFunctions';
import { GenericEntityGenerated } from './GenericEntityGenerated';

/**
 * @class: TimedEntity - This class will be extended by entities
 * @extends GenericEntityGenerated
 * @property createdAt: Timestamp - timestamp of the creation of the entity
 * @property updatedAt: Timestamp - timestamp of the last update of the entity
 * @property deletedAt: Timestamp - timestamp of the deletion of the entity
 * @method beforeInsert: void - method called before the insertion of the entity
 */

export class TimedEntity extends GenericEntityGenerated {
  @CreateDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.currentTimestamp,
    name: 'created_at',
  })
  createdAt: Timestamp;
  @UpdateDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.currentTimestamp,
    name: 'updated_at',
  })
  updatedAt: Timestamp;
  @DeleteDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.defaultNull,
    name: 'deleted_at',
  })
  deletedAt: Timestamp;
}
