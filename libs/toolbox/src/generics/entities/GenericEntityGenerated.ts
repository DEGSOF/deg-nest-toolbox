import { BaseEntity, PrimaryGeneratedColumn } from 'typeorm';

/**
 * @class: GenericEntityGenerated - This class will be extended by entities
 * @extends BaseEntity
 * @property id: number - id of the entity
 */

export class GenericEntityGenerated extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
}
