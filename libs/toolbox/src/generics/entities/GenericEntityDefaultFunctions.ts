export abstract class EntityDefaultFunctions {
  public static defaultNull = (): string => 'NULL';
  public static defaultZero = (): string => '0';
  public static currentTimestamp = (): string => 'CURRENT_TIMESTAMP(6)';
}
