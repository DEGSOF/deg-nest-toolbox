export type FindOffsetPaginationResponse = {
  records: any[];
  count: number;
  total: number;
  nextOffset: number;
  previousOffset: number;
};
