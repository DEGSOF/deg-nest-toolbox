import { PageInfo } from './page-info.type';

export class FindCursorResponse {
  totalCount: number;
  pageInfo: PageInfo;
  edges: Edge[];
}

export type Node = {
  [key: string]: any;
};

export type Edge = {
  cursor: string;
  node: Node;
};
