// Generated by GitHub Copilot
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { BaseEntity, FindManyOptions, RemoveOptions, Repository, SaveOptions } from 'typeorm';

import { GenericRepository } from './GenericRepository.repository';

class MockEntity extends BaseEntity {}

describe('GenericRepository', () => {
  let repository: GenericRepository<MockEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GenericRepository,
        {
          provide: getRepositoryToken(MockEntity),
          useClass: Repository,
        },
      ],
    }).compile();

    repository = module.get<GenericRepository<MockEntity>>(GenericRepository);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });

  describe('find', () => {
    it('should call super.find with the provided options', async () => {
      const options = { where: { id: '1' } } as FindManyOptions<MockEntity>;
      const findSpy = jest.spyOn(Repository.prototype, 'find').mockResolvedValueOnce([]);

      await repository.find(options);

      expect(findSpy).toHaveBeenCalledWith(options);
    });

    it('should return the result of super.find', async () => {
      const result = [{ id: 1 }];
      jest.spyOn(Repository.prototype, 'find').mockResolvedValueOnce(result);
      const findResult = await repository.find();
      expect(findResult).toEqual(result);
    });
  });

  describe('executeRawQuery', () => {
    it('should call this.query with the provided query and parameters', async () => {
      const query = 'SELECT * FROM users WHERE id = ?';
      const parameters = [1];
      const querySpy = jest.spyOn(repository, 'query').mockResolvedValueOnce([]);

      await repository.executeRawQuery(query, parameters);

      expect(querySpy).toHaveBeenCalledWith(query, parameters);
    });

    it('should return the result of this.query', async () => {
      const result = [{ id: 1 }];
      jest.spyOn(repository, 'query').mockResolvedValueOnce(result);
      const queryResult = await repository.executeRawQuery('');
      expect(queryResult).toEqual(result);
    });
  });

  describe('saveRecord', () => {
    it('should call super.save with the provided entity and options', async () => {
      const entity = new MockEntity();
      const options = { chunk: 100 };
      const saveSpy = jest.spyOn(Repository.prototype, 'save').mockResolvedValueOnce(entity);

      await repository.saveRecord(entity, options);

      expect(saveSpy).toHaveBeenCalledWith(entity, options);
    });

    it('should return the result of super.save when entity is not an array', async () => {
      const entity = new MockEntity();
      jest.spyOn(repository, 'save').mockResolvedValueOnce(entity);
      const saveResult = await repository.saveRecord(entity);
      expect(saveResult).toEqual(entity);
    });

    it('should return the result of super.save when entity is an array', async () => {
      const entities = [new MockEntity()];
      jest.spyOn(repository, 'save').mockResolvedValueOnce(entities as any);
      const saveResult = await repository.saveRecord(entities);
      expect(saveResult).toEqual(entities);
    });
  });

  describe('deleteRecord', () => {
    it('should throw an error when no record is found', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(undefined);

      await expect(repository.deleteRecord(criteria)).rejects.toThrow('Record/s not found');
    });

    it('should call super.remove with the found record and options', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      const found = new MockEntity();
      const options = { cascade: true } as RemoveOptions;
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(found);
      const removeSpy = jest.spyOn(Repository.prototype, 'remove').mockResolvedValueOnce(found);

      await repository.deleteRecord(criteria, options);

      expect(removeSpy).toHaveBeenCalledWith(found, options);
    });

    it('should return the result of super.remove', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      const found = new MockEntity();
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(found);
      jest.spyOn(Repository.prototype, 'remove').mockResolvedValueOnce(found);
      const removeResult = await repository.deleteRecord(criteria);
      expect(removeResult).toEqual(found);
    });
  });

  describe('softDeleteRecord', () => {
    it('should throw an error when no record is found', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(undefined);

      await expect(repository.softDeleteRecord(criteria)).rejects.toThrow('Record/s not found');
    });

    it('should call super.softRemove with the found record and options', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      const found = new MockEntity();
      const options = { cascade: true } as SaveOptions;
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(found);
      const softRemoveSpy = jest.spyOn(Repository.prototype, 'softRemove').mockResolvedValueOnce(found);

      await repository.softDeleteRecord(criteria, options);

      expect(softRemoveSpy).toHaveBeenCalledWith(found, options);
    });

    it('should return the result of super.softRemove', async () => {
      const criteria = { where: { id: 1 } } as FindManyOptions<MockEntity>;
      const found = new MockEntity();
      jest.spyOn(repository, 'findOne').mockResolvedValueOnce(found);
      jest.spyOn(repository, 'softRemove').mockResolvedValueOnce(found);
      const softRemoveResult = await repository.softDeleteRecord(criteria);
      expect(softRemoveResult).toBe(found);
    });
  });

  describe('getRepository', () => {
    it('should return this', () => {
      expect(repository.getRepository()).toBe(repository);
    });
  });
});
