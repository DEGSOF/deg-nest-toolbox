import { status } from '@grpc/grpc-js';
import { BadRequestException, Catch, RpcExceptionFilter } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Observable, throwError } from 'rxjs';
import { QueryFailedError } from 'typeorm';

@Catch()
export class GRPCExceptionFilter implements RpcExceptionFilter<any> {
  catch(exception: any): Observable<any> {
    if (exception instanceof BadRequestException) {
      return throwError(() => {
        const errors = exception.getResponse()['message'][0];
        const rpcException = new RpcException({ message: JSON.stringify(errors.constraints), code: status.INVALID_ARGUMENT });
        return rpcException.getError();
      });
    }
    if (exception instanceof QueryFailedError) {
      return throwError(() => {
        const rpcException = new RpcException({ message: exception.message, code: status.INVALID_ARGUMENT });
        return rpcException.getError();
      });
    }
    return throwError(() => {
      const exceptionObject = <object>exception;
      if (exceptionObject['response'] && exceptionObject['response']['status']) {
        if (exceptionObject['response']['data']['error']) {
          if (exceptionObject['response']['data']['error']['message']) {
            const rpcException = new RpcException({ message: JSON.stringify(exceptionObject['response']['data']['error']), code: status.INVALID_ARGUMENT });
            return rpcException.getError();
          } else {
            const rpcException = new RpcException({ message: exceptionObject['response']['data']['error'], code: status.INVALID_ARGUMENT });
            return rpcException.getError();
          }
        }
      }
      return exception;
    });
  }
}
