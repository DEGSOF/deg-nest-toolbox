import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';
import { isArray, isNumber } from 'class-validator';

/**
 * @description: This class is used to handle all the exceptions thrown by the application
 * @class: HttpExceptionFilter
 * @implements ExceptionFilter, GqlExceptionFilter
 */
@Catch()
export class HttpExceptionFilter implements ExceptionFilter, GqlExceptionFilter {
  errors = [];
  async catch(exception: any, host: ArgumentsHost): Promise<void> {
    this.errors = [];
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    /* istanbul ignore next */
    const request = ctx.getRequest<Request>() === undefined ? null : ctx.getRequest<Request>();
    /* istanbul ignore next */
    const statusCode = exception instanceof HttpException ? exception.getStatus() ?? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.INTERNAL_SERVER_ERROR;
    const exceptionObject = <object>exception;
    if (exceptionObject['response']) {
      if (exceptionObject['response']['message']) {
        this.traverse(exceptionObject['response']['message']);
      } else {
        this.traverse(exceptionObject['response']);
      }
    }
    response.status(statusCode).json({
      error: {
        code: statusCode,
        message: exceptionObject['message'],
        timestamp: new Date().toISOString(),
        path: request.url ?? '',
        details: this.errors,
      },
    });
  }
  //recurrent function to sum
  traverse(obj): void {
    if (isArray(obj)) {
      obj.forEach((element) => this.traverse(element));
    } else {
      if (obj['children'] && obj['children'].length > 0) {
        this.errors.push({
          property: obj['property'],
          constraints: obj.children.map((child) => {
            if (isNumber(Number(child.property))) {
              return child['children'].map((grandchild) => {
                return {
                  property: grandchild['property'],
                  constraints: grandchild['constraints'],
                };
              });
            } else {
              /* istanbul ignore next */
              return {
                property: child['property'],
                constraints: child['constraints'],
              };
            }
          }),
        });
      } else {
        if (typeof obj === 'object' && obj !== null) {
          this.errors.push({
            property: obj['property'],
            constraints: obj['constraints'],
          });
        }
        if (typeof obj !== 'object' && obj !== null) {
          this.errors.push({
            property: obj,
            constraints: obj,
          });
        }
      }
    }
  }
}
