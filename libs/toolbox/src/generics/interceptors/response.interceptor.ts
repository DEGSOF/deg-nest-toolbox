import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { instanceToPlain } from 'class-transformer';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export type Response<T> = {
  data: T;
  total: number;
};

/**
 * @description: This class is used to intercept the response of the HTTP endpoints of the application
 * @class: HttpExceptionFilter
 * @param: {ExecutionContext} context
 * @param: {CallHandler} next
 * @returns: {Observable<any>}
 */
@Injectable()
export class ResponseInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    if (context['contextType']) {
      if (context['contextType'] === 'graphql') {
        /* istanbul ignore next */
        return next.handle().pipe(map((data) => data));
      } else {
        /* istanbul ignore next */
        return next.handle().pipe(
          map(async (data) => {
            return {
              data: instanceToPlain(data),
              time: moment().format('YYYY-MM-DD HH:mm:ss'),
              status: context.switchToHttp().getResponse().statusCode,
            };
          }),
        );
      }
    }
  }
}
