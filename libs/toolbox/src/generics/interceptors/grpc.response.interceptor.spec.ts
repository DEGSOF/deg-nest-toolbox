// Generated by GitHub Copilot
import { ExecutionContext } from '@nestjs/common';
import { CallHandler } from '@nestjs/common/interfaces';
import { Test, TestingModule } from '@nestjs/testing';
import { Observable } from 'rxjs';

import { GRPCResponseInterceptor } from './grpc.response.interceptor';

describe('GRPCResponseInterceptor', () => {
  let interceptor: GRPCResponseInterceptor<any>;
  let context: ExecutionContext;
  let next: CallHandler;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GRPCResponseInterceptor],
    }).compile();

    interceptor = module.get<GRPCResponseInterceptor<any>>(GRPCResponseInterceptor);
    context = {
      switchToHttp: jest.fn(),
      switchToRpc: jest.fn(),
      switchToWs: jest.fn(),
      getClass: jest.fn(),
      getHandler: jest.fn(),
      getArgs: jest.fn(),
      getArgByIndex: jest.fn(),
      getType: jest.fn(),
      getArg: jest.fn(),
      getContext: jest.fn(),
      getMethod: jest.fn(),
      getMetadata: jest.fn(),
      getRpcContext: jest.fn(),
      getRpcArguments: jest.fn(),
      getRpcParams: jest.fn(),
    } as unknown as ExecutionContext;
    next = {
      handle: jest.fn(() => new Observable()),
    };
  });

  it('should be defined', () => {
    expect(interceptor).toBeDefined();
  });

  it('should return the data as is if the context type is graphql', () => {
    context['contextType'] = 'graphql';
    const data = { foo: 'bar' };
    jest.spyOn(next, 'handle').mockReturnValueOnce(new Observable((subscriber) => subscriber.next(data)));
    interceptor.intercept(context, next).subscribe((result) => {
      expect(result).toEqual(data);
    });
  });

  it('should return the data as plain object if the context type is not graphql', () => {
    context['contextType'] = 'http';
    const data = { foo: 'bar' };
    jest.spyOn(next, 'handle').mockReturnValueOnce(new Observable((subscriber) => subscriber.next(data)));
    interceptor.intercept(context, next).subscribe((result) => {
      expect(result).toEqual({ data, total: undefined });
    });
  });
});
