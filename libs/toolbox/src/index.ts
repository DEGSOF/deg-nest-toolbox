import { validate } from 'class-validator';

import { CursorPagination, GenericDto } from './generics/dtos/generic.dto';
import { EntityDefaultFunctions } from './generics/entities/GenericEntityDefaultFunctions';
import { GenericEntityGenerated } from './generics/entities/GenericEntityGenerated';
import { GenericEntityLexic } from './generics/entities/GenericEntityLexic';
import { GenericEntityUUID } from './generics/entities/GenericEntityUUID';
import { LabedEntity } from './generics/entities/LabedEntity';
import { LabedEntityLexic } from './generics/entities/LabedEntityLexic';
import { TimedEntity } from './generics/entities/TimedEntity';
import { TimedEntityWithLexicId } from './generics/entities/TimedEntityWithLexicId';
import { controllerFactory } from './generics/factories/ControllerFactory.factory';
import { HttpExceptionFilter } from './generics/filters/deg-exceptions-handler.filter';
import { DEGGqlExceptionFilter } from './generics/filters/deg-gql-exceptions-handler.filter';
import { GRPCExceptionFilter } from './generics/filters/deg-grpc-exceptions.filter';
import { GenericRepository } from './generics/GenericRepository.repository';
import { GenericService } from './generics/GenericService.service';
import { GRPCResponseInterceptor } from './generics/interceptors/grpc.response.interceptor';
import { ResponseInterceptor } from './generics/interceptors/response.interceptor';
import { GRPCSearchInterface } from './generics/interfaces/GRPCSearchInterface.interface';
import { ICrudController } from './generics/interfaces/ICrudController.interface';
import { AbstractValidationPipe } from './generics/pipes/AbstractValidationPipe.pipe';
import { injectUserTo, injectUserToBody, injectUserToParam, injectUserToQuery } from './generics/request/decorators/inject.user.decorator';
import { InjectUserInterceptor, REQUEST_CONTEXT } from './generics/request/interceptors/inject.user.interceptor';
import { StripRequestContextPipe } from './generics/request/pipes/strip.request.context.pipe';
import { TOBOOLEAN } from './generics/transformers/toBoolean';
import { toSlugCode, valueToSlugCode } from './generics/transformers/toSlugCode';
import { FindOffsetPaginationResponse } from './generics/types/find-offset-pagination-response-type';
import { FindFilter, FindRequest, Match, SearchTerm } from './generics/types/find-request.type';
import { Edge, FindCursorResponse, Node } from './generics/types/find-response.type';
import { PageInfo } from './generics/types/page-info.type';
import { entireArrayExist, EntireArrayExistRule } from './generics/validators/deg-entireArrayExists';
import { isDateInFormat } from './generics/validators/deg-IsDateInFormat';
import { isDoublePrecision } from './generics/validators/deg-IsDoublePrecision';
import { isNotBlank } from './generics/validators/deg-isNotBlank';
import { isValidCuitCuil } from './generics/validators/deg-IsValidCuit';

export {
  TOBOOLEAN,
  EntireArrayExistRule,
  entireArrayExist,
  validate,
  isDateInFormat,
  isDoublePrecision,
  isNotBlank,
  isValidCuitCuil,
  valueToSlugCode,
  ResponseInterceptor,
  HttpExceptionFilter,
  AbstractValidationPipe,
  GenericEntityGenerated,
  GenericEntityUUID,
  GenericEntityLexic,
  LabedEntity,
  LabedEntityLexic,
  TimedEntityWithLexicId,
  TimedEntity,
  controllerFactory,
  GenericService,
  GenericRepository,
  GenericDto,
  InjectUserInterceptor,
  REQUEST_CONTEXT,
  GRPCExceptionFilter,
  GRPCResponseInterceptor,
  injectUserTo,
  injectUserToBody,
  injectUserToParam,
  injectUserToQuery,
  DEGGqlExceptionFilter,
  FindRequest,
  FindFilter,
  SearchTerm,
  CursorPagination,
  PageInfo,
  EntityDefaultFunctions,
  ICrudController,
  GRPCSearchInterface,
  StripRequestContextPipe,
  toSlugCode,
  FindOffsetPaginationResponse,
  Match,
  FindCursorResponse,
  Edge,
  Node,
};
