import { Catch, ExceptionFilter } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';
import { isArray, isNumber } from 'class-validator';

@Catch()
export class DEGGqlExceptionFilter implements ExceptionFilter, GqlExceptionFilter {
  errors = [];
  async catch(exception: any): Promise<void> {
    this.errors = [];
    const exceptionObject = <object>exception;
    if (exceptionObject['response']) {
      if (exceptionObject['response']['message']) {
        this.traverse(exceptionObject['response']['message']);
      } else {
        this.traverse(exceptionObject['response']);
      }
    }
    if (this.errors.length > 0) {
      exception.response.message = this.errors;
    }
    return exception;
  }
  //recurrent function to sum
  traverse(obj): void {
    if (isArray(obj)) {
      obj.forEach((element) => this.traverse(element));
    } else {
      if (obj['children'] && obj['children'].length > 0) {
        this.errors.push({
          property: obj['property'],
          constraints: obj.children.map((child) => {
            if (isNumber(Number(child.property))) {
              return child['children'].map((grandchild) => {
                return {
                  property: grandchild['property'],
                  constraints: grandchild['constraints'],
                };
              });
            } else {
              return {
                property: child['property'],
                constraints: child['constraints'],
              };
            }
          }),
        });
      } else {
        if (typeof obj === 'object' && obj !== null) {
          this.errors.push({
            property: obj['property'],
            constraints: obj['constraints'],
          });
        }
        if (typeof obj !== 'object' && obj !== null) {
          this.errors.push({
            property: obj,
            constraints: obj,
          });
        }
      }
    }
  }
}
