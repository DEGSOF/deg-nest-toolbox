// Generated by GitHub Copilot
import { status } from '@grpc/grpc-js';
import { BadRequestException } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { QueryFailedError } from 'typeorm';

import { GRPCExceptionFilter } from './deg-grpc-exceptions.filter';

describe('GRPCExceptionFilter', () => {
  let filter: GRPCExceptionFilter;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GRPCExceptionFilter],
    }).compile();

    filter = module.get<GRPCExceptionFilter>(GRPCExceptionFilter);
  });

  it('should be defined', () => {
    expect(filter).toBeDefined();
  });

  it('should be defined', () => {
    expect(filter).toBeDefined();
  });
});
// Generated by GitHub Copilot
describe('GRPCExceptionFilter', () => {
  let filter: GRPCExceptionFilter;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GRPCExceptionFilter],
    }).compile();

    filter = module.get<GRPCExceptionFilter>(GRPCExceptionFilter);
  });

  it('should catch BadRequestException and return RpcException with INVALID_ARGUMENT code', () => {
    const exception = new BadRequestException('Test message', 'Test context');
    const expectedError = new RpcException({ message: JSON.stringify(exception.getResponse()['message'][0].constraints), code: status.INVALID_ARGUMENT });

    filter.catch(exception).subscribe({
      error: (error) => {
        expect(error).toEqual(expectedError.getError());
      },
    });
  });

  it('should catch QueryFailedError and return RpcException with INVALID_ARGUMENT code', () => {
    const exception = new QueryFailedError('Test message', [], 'Test query');
    const expectedError = new RpcException({ message: exception.message, code: status.INVALID_ARGUMENT });

    filter.catch(exception).subscribe({
      error: (error) => {
        expect(error).toEqual(expectedError.getError());
      },
    });
  });

  it('should catch any other exception and return the same exception', () => {
    const exception = new Error('Test message');
    filter.catch(exception).subscribe({
      error: (error) => {
        expect(error).toEqual(exception);
      },
    });
  });

  it('should catch any other exception and return the same exception', () => {
    const exception = {
      response: {
        status: 400,
        data: {
          error: {
            message: 'Test message',
          },
        },
      },
    };
    filter.catch(exception).subscribe({
      error: (error) => {
        expect(error).toEqual(exception);
      },
    });
  });

  it('should catch any other exception and return the same exception', () => {
    const exception = {
      response: {
        status: 400,
        data: {
          error: 'Test message',
        },
      },
    };
    filter.catch(exception).subscribe({
      error: (error) => {
        expect(error).toEqual(exception);
      },
    });
  });
});
