export type FindRequest = {
  first?: number;
  after?: string;
  last?: number;
  before?: string;
  filters?: FindFilter[];
  searchTerms?: SearchTerm[];
};

export type FindFilter = {
  field?: string;
  value?: string;
};

export enum Match {
  LIKE = 'LIKE',
  EXACT = 'EXACT',
}

export type SearchTerm = {
  fields?: string[];
  value?: string;
  match?: Match;
};
