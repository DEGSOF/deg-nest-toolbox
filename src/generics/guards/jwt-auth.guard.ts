import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

/**
 * @description: This class is used to guard the routes that need authentication with JWT on REST endpoints of the application
 * @class: HttpExceptionFilter
 * @extends AuthGuard
 * @property reflector: Reflector - used to get the metadata of the route
 * @method canActivate: boolean - method called to check if the route can be activated
 */
@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  public constructor(private readonly reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext): any {
    const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler());
    if (isPublic) {
      return true;
    }
    /* istanbul ignore next */
    return super.canActivate(context);
  }

  handleRequest(err, user): UnauthorizedException | any {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
