import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

/**
 * @description: This class is used to guard the routes that need authentication with JWT on GRPC endpoints of the application
 * @class: HttpExceptionFilter
 * @implements CanActivate
 * @property jwtServ: JwtService - used to decode the JWT token
 * @method getRequest: any - method called to get the request
 * @method canActivate: boolean - method called to check if the route can be activated
 * @param: {ExecutionContext} context
 * @returns: {Promise<boolean>}
 * @returns: {boolean}
 */
@Injectable()
export class JwtAuthGRPCGuard implements CanActivate {
  public constructor(private readonly jwtServ: JwtService) {}

  getRequest(context: ExecutionContext): any {
    return context.switchToRpc().getContext();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = this.getRequest(context);

    const type = context.getType();
    const prefix = 'Bearer ';
    let accountData = null;
    let header: any = null;
    if (type === 'rpc') {
      const metadata = context.getArgByIndex(1);
      if (!metadata) {
        return false;
      }
      header = metadata.get('Authorization')[0];
    }
    if (!header) {
      return false;
    }
    if (!header.includes(prefix)) {
      return false;
    }
    const token = header.slice(header.indexOf(' ') + 1);
    accountData = this.jwtServ.decode(token);
    request.set('accountData', accountData);
    return accountData ?? false;
  }
}
