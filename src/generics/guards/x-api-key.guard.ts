import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';

/**
 * @description: This class is used to guard the routes that need authentication with x-api-key on REST endpoints of the application
 * @class: HttpExceptionFilter
 * @extends AuthGuard
 * @property reflector: Reflector - used to get the metadata of the route
 * @method canActivate: boolean - method called to check if the route can be activated
 */
@Injectable()
export class XApiKeyGuard extends AuthGuard('xapi') {
  /* istanbul ignore next */
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    return super.canActivate(context);
  }

  handleRequest(err, user): UnauthorizedException | any {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
