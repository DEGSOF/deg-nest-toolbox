import { Match } from '../types/find-request.type';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export abstract class GRPCSearchInterface {
  abstract prepareFilters(filters: [{ field: string; value: string }]): Array<object>;
  abstract prepareSearch(search: [{ fields: string[]; value: string; match: Match }]): Array<object>;
}
