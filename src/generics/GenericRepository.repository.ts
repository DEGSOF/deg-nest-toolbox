import { BaseEntity, FindManyOptions, FindOneOptions, RemoveOptions, Repository, SaveOptions } from 'typeorm';

export class GenericRepository<_Entity extends BaseEntity & { id?: any }> extends Repository<_Entity> {
  /**
   * @function: find - This method will return a list of records
   * @param options: FindManyOptions - options to be applied to the query from typeorm
   * @returns Promise<_Entity[]>
   */

  async find(options?: FindManyOptions<_Entity>): Promise<_Entity[]> {
    return await super.find(options);
  }

  /**
   * @function: executeRawQuery - This method will return raw query result
   * @param query: string - query to be executed
   * @param parameters: any[] - parameters to be applied to the query
   * @returns Promise<any>
   */

  async executeRawQuery(query: string, parameters?: any[]): Promise<any> {
    return await this.query(query, parameters);
  }

  /**
   * @function: saveRecord - This method will save a record in the database
   * @param entity: _Entity | _Entity[] must extend from BaseEntity and have an id property from typeorm
   * @param options: SaveOptions - options to be applied to the query from typeorm
   * @returns a single entity or an array of entities
   */

  async saveRecord(
    entity: _Entity | _Entity[],
    options: SaveOptions = {
      chunk: 100,
      data: null,
      listeners: true,
      reload: true,
      transaction: true,
    },
  ): Promise<_Entity | _Entity[]> {
    if (Array.isArray(entity)) {
      return await this.save(entity, options);
    }
    return await this.save(entity, options);
  }

  /**
   * @function: deleteRecord - This method will delete a record from the database (hard delete)
   * @param criteria: FindOneOptions<_Entity> - options to be applied to the query from typeorm
   * @param options: RemoveOptions - options to be applied to the query from typeorm
   * @returns: a single entity or an array of entities
   */

  async deleteRecord(criteria: FindOneOptions<_Entity>, options?: RemoveOptions): Promise<_Entity | _Entity[]> {
    const found = await this.findOne(criteria);
    if (!found) {
      throw new Error('Record/s not found');
    }
    return await this.remove(found, options);
  }

  /**
   * @function: softDeleteRecord - This method will soft delete a record from the database
   * @param criteria: FindOneOptions<_Entity> - options to be applied to the query from typeorm
   * @param options: SaveOptions - options to be applied to the query from typeorm
   * @returns: a single entity or an array of entities
   */

  async softDeleteRecord(criteria: FindOneOptions<_Entity>, options?: SaveOptions): Promise<_Entity | _Entity[]> {
    const found = await this.findOne(criteria);
    if (!found) {
      throw new Error('Record/s not found');
    }
    return await this.softRemove(found, options);
  }

  /**
   * @function: getRepository - This method will return the repository
   * @returns this
   */

  getRepository(): this {
    return this;
  }
}
