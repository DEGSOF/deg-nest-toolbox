/**
 * Defines a TypeScript decorator function called `TOBOOLEAN` that transforms a class property into a boolean value.
 * This decorator uses the `class-transformer` library to perform the transformation.
 *
 * @returns {Function} The decorator function.
 */
import { Transform } from 'class-transformer';

const TOBOOLEAN = (): any => {
  const toPlain = Transform(({ value }) => value, { toPlainOnly: true });

  /**
   * Transforms the property value to a boolean during deserialization.
   *
   * @param {any} target - The target object (class instance).
   * @param {string} key - The name of the property.
   * @returns {void}
   */
  const toClass = (target: any, key: string): void => {
    return Transform(({ obj }) => valueToBoolean(obj[key]), { toClassOnly: true })(target, key);
  };

  return function (target: any, key: string): void {
    toPlain(target, key);
    toClass(target, key);
  };
};

/**
 * Converts a value to a boolean.
 *
 * @param {any} value - The value to convert.
 * @returns {boolean} The converted boolean value.
 */
const valueToBoolean = (value: any): boolean => {
  if (value === null || value === undefined) {
    return undefined;
  }

  if (typeof value === 'boolean') {
    return value;
  }

  if (['true', 'on', 'yes', '1'].includes(value.toLowerCase())) {
    return true;
  }

  if (['false', 'off', 'no', '0'].includes(value.toLowerCase())) {
    return false;
  }

  return false;
};

export { TOBOOLEAN, valueToBoolean };
