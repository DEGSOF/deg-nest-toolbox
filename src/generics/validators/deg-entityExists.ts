import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { DataSource } from 'typeorm';

@ValidatorConstraint({ name: 'EntityExistRule', async: true })
@Injectable()
export class EntityExistRule implements ValidatorConstraintInterface {
  constructor(@InjectDataSource() private readonly connection: DataSource) {}

  async validate(value: number, args: ValidationArguments): Promise<boolean> {
    const [entity, column] = args.constraints;
    const repository = this.connection.getRepository(entity);
    try {
      const record = await repository.findOne({ where: { [column]: value } });
      return record !== null;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}

export function entityExist(entity: object, column: string, validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string): void {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, column],
      validator: EntityExistRule,
    });
  };
}
