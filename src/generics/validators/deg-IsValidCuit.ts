import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ async: true })
class IsValidCuitCuilConstraint implements ValidatorConstraintInterface {
  async validate(value: any | Array<any>): Promise<boolean> {
    if (!value) return false;
    if (value.length != 11) return false;
    let rv = false;
    let resultado = 0;
    const cuitNro = value.toString().replace(/-/g, '').replace(/ /g, '').replace(/_/g, '').replace(/\./g, '').replace(/\//g, '');
    const codes = '6789456789';
    const verificador = parseInt(cuitNro[cuitNro.length - 1]);
    let x = 0;

    while (x < 10) {
      let digitoValidador = parseInt(codes.substring(x, x + 1));
      /* istanbul ignore next */
      if (isNaN(digitoValidador)) digitoValidador = 0;
      let digito = parseInt(cuitNro.substring(x, x + 1));
      if (isNaN(digito)) digito = 0;
      const digitoValidacion = digitoValidador * digito;
      resultado += digitoValidacion;
      x++;
    }

    resultado = resultado % 11;
    rv = resultado == verificador;
    return rv;
  }

  defaultMessage(args: ValidationArguments): string {
    const property = args.property;
    return `${property} es invalido.`;
  }
}

export function isValidCuitCuil(validationOptions?: ValidationOptions) {
  return function (object: Record<string, any>, propertyName: string): void {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsValidCuitCuilConstraint,
    });
  };
}
