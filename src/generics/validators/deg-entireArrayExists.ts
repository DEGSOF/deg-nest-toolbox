import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { isArray, registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { DataSource, In } from 'typeorm';

@ValidatorConstraint({ name: 'EntireArrayExistRule', async: true })
@Injectable()
export class EntireArrayExistRule implements ValidatorConstraintInterface {
  constructor(@InjectDataSource() private readonly connection: DataSource) {}

  async validate(value: any, args: ValidationArguments): Promise<boolean> {
    const [entity] = args.constraints;
    const repository = this.connection.getRepository(entity);
    if (!isArray(value)) {
      return false;
    }
    if (value.length == 0) {
      return false;
    }
    try {
      value = value.map((item) => item.id);
      const result = await repository
        .createQueryBuilder()
        .select('count(*)', 'cantidad')
        .where({
          id: In(value),
        })
        .execute();
      if (result?.[0]) {
        if (result[0].cantidad == value.length) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
export function entireArrayExist(entity, column: string, validationOptions?: ValidationOptions): (object: any, propertyName: string) => void {
  return function (object: any, propertyName: string): void {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [entity, column],
      validator: EntireArrayExistRule,
    });
  };
}
