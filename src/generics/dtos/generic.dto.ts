import { Expose } from 'class-transformer';
import { IsObject, IsOptional } from 'class-validator';

import { PageInfo } from './../../interfaces/find-settings';

export class GenericDto {
  @IsOptional()
  @IsObject()
  requestContext: any;
}

export class CursorPagination {
  edges: any[];
  @Expose({ name: 'page_info', toPlainOnly: true })
  pageInfo: PageInfo;
  @Expose({ name: 'total_count', toPlainOnly: true })
  totalCount: number;
}
