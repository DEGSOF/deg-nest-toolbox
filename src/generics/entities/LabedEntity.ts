import { Column } from 'typeorm';

import { TimedEntity } from './TimedEntity';

/**
 * @class: LabedEntity - This class will be extended by entities
 * @extends TimedEntity
 * @property label: string - label of the entity
 * @property code: string - code of the entity
 * @property id: number - id of the entity
 * @property createdAt: Date - creation date of the entity
 * @property updatedAt: Date - last update date of the entity
 * @property deletedAt: Date - deletion date of the entity
 */

export class LabedEntity extends TimedEntity {
  @Column({ type: 'varchar', length: 128 })
  label: string;
  @Column({ type: 'varchar', length: 128 })
  code: string;
}
