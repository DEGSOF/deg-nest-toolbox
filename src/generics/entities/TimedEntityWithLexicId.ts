import { Expose } from 'class-transformer';
import { CreateDateColumn, DeleteDateColumn, Timestamp, UpdateDateColumn } from 'typeorm';

import { EntityDefaultFunctions } from './GenericEntityDefaultFunctions';
import { GenericEntityLexic } from './GenericEntityLexic';

/**
 * @class: TimedEntityWithLexicId - This class will be extended by entities
 * @extends GenericEntityLexic
 * @property createdAt: Timestamp - timestamp of the creation of the entity
 * @property updatedAt: Timestamp - timestamp of the last update of the entity
 * @property deletedAt: Timestamp - timestamp of the deletion of the entity
 * @method beforeInsert: void - method called before the insertion of the entity
 */

export class TimedEntityWithLexicId extends GenericEntityLexic {
  @Expose({
    name: 'created_at',
  })
  @CreateDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.currentTimestamp,
    name: 'created_at',
  })
  createdAt: Timestamp;
  @Expose({
    name: 'updated_at',
  })
  @UpdateDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.currentTimestamp,
    name: 'updated_at',
  })
  updatedAt: Timestamp;
  @Expose({
    name: 'deleted_at',
  })
  @DeleteDateColumn({
    type: 'timestamp',
    default: EntityDefaultFunctions.defaultNull,
    name: 'deleted_at',
  })
  deletedAt: Timestamp;
}
