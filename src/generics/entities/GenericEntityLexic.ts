import { BaseEntity, BeforeInsert, PrimaryColumn } from 'typeorm';
import { ulid } from 'ulid';

/**
 * @class: GenericEntityLexic - This class will be extended by entities
 * @extends BaseEntity
 * @property id: string - id of the entity (ulid)
 */

export class GenericEntityLexic extends BaseEntity {
  @PrimaryColumn({ type: 'char', length: 26 })
  id: string;

  @BeforeInsert()
  beforeInsert(): void {
    if (!this.id) {
      this.id = ulid().toString();
    }
  }
}
