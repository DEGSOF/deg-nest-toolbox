import { Column } from 'typeorm';

import { TimedEntityWithLexicId } from './TimedEntityWithLexicId';

/**
 * @class: LabedEntityLexic - This class will be extended by entities
 * @extends TimedEntityWithLexicId
 * @property label: string - label of the entity
 * @property code: string - code of the entity
 * @property id: string - id of the entity (ulid)
 * @property createdAt: Date - creation date of the entity
 * @property updatedAt: Date - last update date of the entity
 * @property deletedAt: Date - deletion date of the entity
 * @method beforeInsert: void - method called before the insertion of the entity
 */

export class LabedEntityLexic extends TimedEntityWithLexicId {
  @Column({ type: 'varchar', length: 128 })
  label: string;
  @Column({ type: 'varchar', length: 128 })
  code: string;
}
