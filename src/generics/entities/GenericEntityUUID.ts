import { BaseEntity, BeforeInsert, PrimaryColumn } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

/**
 * @class: GenericEntityUUID - This class will be extended by entities
 * @extends BaseEntity
 * @property id: string - id of the entity (uuid)
 */

export class GenericEntityUUID extends BaseEntity {
  @PrimaryColumn({ type: 'char', length: 26 })
  id: string;

  @BeforeInsert()
  beforeInsert(): void {
    if (!this.id) {
      this.id = uuidv4().toString();
    }
  }
}
