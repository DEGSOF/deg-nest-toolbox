// Generated by GitHub Copilot
import { ExecutionContext } from '@nestjs/common';
import { CallHandler } from '@nestjs/common/interfaces';
import { Test, TestingModule } from '@nestjs/testing';
import { Observable } from 'rxjs';

import { ResponseInterceptor } from './response.interceptor';

describe('ResponseInterceptor', () => {
  let interceptor: ResponseInterceptor<any>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResponseInterceptor],
    }).compile();

    interceptor = module.get<ResponseInterceptor<any>>(ResponseInterceptor);
  });

  describe('intercept', () => {
    it('should return the data as is for GraphQL context', async () => {
      const context: ExecutionContext = {
        contextType: 'graphql',
      } as unknown as ExecutionContext;
      const next: CallHandler = {
        handle: () => {
          return {
            pipe: (): any => {
              return {
                map: (): any => {
                  return {
                    toPromise: (): any => {
                      return Promise.resolve('data');
                    },
                  };
                },
              };
            },
          };
        },
      } as CallHandler;
      const result: Observable<any> = interceptor.intercept(context, next);
      expect(result).toBeInstanceOf(Object);
      expect(result).toEqual({
        map: expect.any(Function),
      });
    });

    it('should return the data with metadata for non-GraphQL context', async () => {
      const context: ExecutionContext = {
        contextType: 'http',
        switchToHttp: (): any => {
          return {
            getResponse: (): any => {
              return {
                statusCode: 200,
              };
            },
          };
        },
      } as unknown as ExecutionContext;
      const next: CallHandler = {
        handle: () => {
          return {
            pipe: (): any => {
              return {
                toPromise: (): any => {
                  return Promise.resolve('data');
                },
              };
            },
          };
        },
      } as CallHandler;
      const result: Observable<any> = interceptor.intercept(context, next);
      expect(result).toBeInstanceOf(Object);
      expect(result).toBeInstanceOf(Object);
    });
  });
});
