import { applyDecorators, UseInterceptors, UsePipes } from '@nestjs/common';

import { InjectUserInterceptor } from './../interceptors/inject.user.interceptor';
import { StripRequestContextPipe } from './../pipes/strip.request.context.pipe';

/**
 * @description: This decorator is used to inject the user in the request query
 * @returns {any}
 */

export function injectUserToQuery(): any {
  return applyDecorators(injectUserTo('query'));
}

/**
 * @description: This decorator is used to inject the user in the request body
 * @returns {any}
 */

export function injectUserToBody(): any {
  return applyDecorators(injectUserTo('body'));
}

/**
 * @description: This decorator is used to inject the user in the request params
 * @returns {any}
 */

export function injectUserToParam(): any {
  return applyDecorators(injectUserTo('params'));
}

/**
 * @description: This decorator is used to inject the user in the request
 * @param {string} context
 * @returns {any}
 */

export function injectUserTo(context: 'query' | 'body' | 'params'): any {
  return applyDecorators(UseInterceptors(new InjectUserInterceptor(context)), UsePipes(StripRequestContextPipe));
}
