import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';

export const REQUEST_CONTEXT = 'requestContext';

/**
 * @description: This class is used to intercept the request of the endpoints of the application and inject the user in the request
 * @class: InjectUserInterceptor - implements NestInterceptor
 * @param: {ExecutionContext} context
 * @param: {CallHandler} next
 * @returns: {Observable<any>}
 */
@Injectable()
export class InjectUserInterceptor implements NestInterceptor {
  constructor(private type?: NonNullable<'query' | 'body' | 'params'>) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();

    /* istanbul ignore next */
    if (this.type && request[this.type]) {
      request[this.type][REQUEST_CONTEXT] = {
        user: request.user,
      };
    }

    return next.handle();
  }
}
