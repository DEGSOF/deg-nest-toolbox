import { Injectable, PipeTransform } from '@nestjs/common';
import { omit } from 'lodash';

import { REQUEST_CONTEXT } from './../interceptors/inject.user.interceptor';

/**
 * @description: This class is used to strip the request context from the request
 * @class: StripRequestContextPipe - implements PipeTransform
 * @param: {any} value
 * @returns: {any}
 */
@Injectable()
export class StripRequestContextPipe implements PipeTransform {
  transform(value: any): any {
    return omit(value, REQUEST_CONTEXT);
  }
}
