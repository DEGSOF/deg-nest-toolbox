import { Column, Entity } from 'typeorm';

import { TimedEntityWithLexicId } from './../../../generics/entities/TimedEntityWithLexicId';

@Entity('users')
export class User extends TimedEntityWithLexicId {
  @Column({ length: 500 })
  name: string;
}
