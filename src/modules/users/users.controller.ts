import { status } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod, RpcException } from '@nestjs/microservices';
import { instanceToPlain } from 'class-transformer';

import { GRPCSearchInterface } from './../../generics/interfaces/GRPCSearchInterface.interface';
import { Match } from './../../generics/types/find-request.type';
import { FindSettings } from './../../interfaces/find-settings';
import { DeleteElementRequest, FindToolsResponse } from './../../interfaces/tools';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';

@Controller()
export class UsersController extends GRPCSearchInterface {
  constructor(private readonly usersService: UsersService) {
    super();
  }

  @GrpcMethod('Tools', 'FindToolSettings')
  async findToolSettings(): Promise<FindSettings> {
    const settings: FindSettings = {
      filters: [
        {
          field: 'name',
          text: 'Filtros',
          options: [],
          multi: false,
        },
      ],
      searchableFields: [],
      orders: [],
    };
    return settings;
  }

  @GrpcMethod('Tools', 'FindTool')
  async findTool(data: any): Promise<FindToolsResponse> {
    const filters = this.prepareFilters(data.filters);
    const search = this.prepareSearch(data.search);
    const response = await this.usersService.findCursorPagination(data, true, filters, search, []);
    //transform the response to the grpc response typed as FindToolsResponse
    return response as FindToolsResponse;
  }

  @GrpcMethod('Tools', 'SetTool')
  async setTool(body: CreateUserDto): Promise<any> {
    let user = null;
    if (body.id) {
      user = await this.usersService.findOne({
        where: {
          id: body.id,
        },
      });
      if (!user) {
        throw new RpcException({ message: 'User not found', code: status.NOT_FOUND });
      }
    }
    user = Object.assign(user, body);
    user = await this.usersService.saveRecord(user);
    return instanceToPlain(user);
  }

  @GrpcMethod('Tools', 'SoftDeleteTool')
  async softDeleteTool(data: DeleteElementRequest): Promise<any> {
    const user = await this.usersService.findOne({
      where: {
        id: data.id,
      },
    });
    if (!user) {
      throw new RpcException({ message: 'User not found', code: status.NOT_FOUND });
    }
    await this.usersService.deleteRecord({
      where: {
        id: data.id,
      },
    });
    return instanceToPlain(user);
  }

  @GrpcMethod('Tools', 'HardDeleteTool')
  async hardDeleteTool(data: DeleteElementRequest): Promise<any> {
    const user = await this.usersService.findOne({
      where: {
        id: data.id,
      },
    });
    if (!user) {
      throw new RpcException({ message: 'User not found', code: status.NOT_FOUND });
    }
    await this.usersService.softDeleteRecord({
      where: {
        id: data.id,
      },
    });
    return instanceToPlain(user);
  }

  prepareFilters(filters: { field: string; value: string }[]): Array<object> {
    filters = filters.map((filter) => filter);
    return filters;
  }

  prepareSearch(search: { fields: string[]; value: string; match: Match }[]): Array<object> {
    search = search.map((searchTerm) => searchTerm);
    return search;
  }
}
