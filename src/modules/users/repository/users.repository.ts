import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from '../entities/user.entity';
import { GenericRepository } from './../../../generics/GenericRepository.repository';

@Injectable()
export class UserRepository extends GenericRepository<User> {
  constructor(
    @InjectRepository(User)
    private repo: GenericRepository<User>,
  ) {
    super(repo.target, repo.manager, repo.queryRunner);
  }
}
