// Generated by GitHub Copilot
import { status } from '@grpc/grpc-js';
import { RpcException } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';

import { FindSettings } from './../../interfaces/find-settings';
import { Match } from './../../schema';
import { UserRepository } from './repository/users.repository';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(UserRepository),
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  describe('are defined', () => {
    it('controller', () => {
      expect(controller).toBeDefined();
    });
    it('service', () => {
      expect(service).toBeDefined();
    });
  });

  describe('setTool', () => {
    it('should update name', async () => {
      const body: any = {
        id: '1',
        name: 'Doe',
      };
      const edited = {
        id: '1',
        name: 'John EDITADO',
      } as any;
      // mock the service
      jest.spyOn(service, 'findOne').mockImplementation(() => edited);
      jest.spyOn(service, 'saveRecord').mockImplementation(() => edited);
      const result = await controller.setTool(body);
      // expect the result
      expect(result).toStrictEqual(edited);
    });
    it('should throw an RpcException if the user is not found', async () => {
      const body: any = {
        id: '321',
        name: 'John',
      };
      jest.spyOn(service, 'findOne').mockReturnValue(null);
      await expect(controller.setTool(body)).rejects.toThrow(new RpcException({ message: 'User not found', code: status.NOT_FOUND }));
    });
  });

  describe('softDeleteTool', () => {
    it('should throw an RpcException if the user is not found', async () => {
      const body: any = {
        id: '321',
      };
      jest.spyOn(service, 'findOne').mockReturnValue(null);
      await expect(controller.softDeleteTool(body)).rejects.toThrow(new RpcException({ message: 'User not found', code: status.NOT_FOUND }));
    });

    it('should return the deleted user', async () => {
      const body: any = {
        id: '1',
      };
      const deleted = {
        id: '1',
        name: 'John',
      } as any;
      jest.spyOn(service, 'findOne').mockImplementation(() => deleted);
      jest.spyOn(service, 'deleteRecord').mockImplementation(() => deleted);
      const result = await controller.softDeleteTool(body);
      expect(result).toStrictEqual(deleted);
    });
  });

  describe('hardDeleteTool', () => {
    it('should throw an RpcException if the user is not found', async () => {
      const body: any = {
        id: '321',
      };
      jest.spyOn(service, 'findOne').mockReturnValue(null);
      await expect(controller.hardDeleteTool(body)).rejects.toThrow(new RpcException({ message: 'User not found', code: status.NOT_FOUND }));
    });

    it('should return the deleted user', async () => {
      const body: any = {
        id: '1',
      };
      const deleted = {
        id: '1',
        name: 'John',
      } as any;
      jest.spyOn(service, 'findOne').mockImplementation(() => deleted);
      jest.spyOn(service, 'softDeleteRecord').mockImplementation(() => deleted);
      const result = await controller.hardDeleteTool(body);
      expect(result).toStrictEqual(deleted);
    });
  });

  describe('FindToolSettings', () => {
    it('it should return findSettings', async () => {
      const settings: FindSettings = {
        filters: [
          {
            field: 'name',
            text: 'Filtros',
            options: [],
            multi: false,
          },
        ],
        searchableFields: [],
        orders: [],
      };
      const result = await controller.findToolSettings();
      expect(result).toStrictEqual(settings);
    });
  });

  describe('filters', () => {
    it('it should return prepareFilters', async () => {
      const filters = [
        {
          field: 'name',
          value: 'John',
        },
      ];
      const result = controller.prepareFilters(filters);
      expect(result).toStrictEqual(filters);
    });

    it('it should return prepareSearch', async () => {
      const search: { fields: string[]; value: string; match: Match }[] = [
        {
          fields: ['name'],
          value: 'John',
          match: Match.EXACT,
        },
      ];
      const result = controller.prepareSearch(search);
      expect(result).toStrictEqual(search);
    });
  });

  describe('FindTool', () => {
    it('it should return findTool', async () => {
      const filters = [
        {
          field: 'name',
          value: 'John',
        },
      ];
      const search: { fields: string[]; value: string; match: Match }[] = [
        {
          fields: ['name'],
          value: 'John',
          match: Match.EXACT,
        },
      ];
      const data = {
        filters,
        search,
      };
      const response = await Promise.resolve({
        edges: [],
        pageInfo: {
          endCursor: null,
          hasNextPage: false,
          hasPreviousPage: false,
          startCursor: null,
        },
        totalCount: 0,
      });
      jest.spyOn(service, 'findCursorPagination').mockImplementation(async () => await response);
      const result = await controller.findTool(data);
      expect(result).toStrictEqual(response);
    });
  });
});
