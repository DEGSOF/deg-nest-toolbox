import { Inject, Injectable } from '@nestjs/common';

import { GenericService } from './../../generics/GenericService.service';
import { User } from './entities/user.entity';
import { UserRepository } from './repository/users.repository';

@Injectable()
export class UsersService extends GenericService<User> {
  constructor(
    @Inject(UserRepository)
    private readonly repository: UserRepository,
  ) {
    super(repository);
  }
}
