
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export enum Match {
    LIKE = "LIKE",
    EXACT = "EXACT"
}

export interface FilterInput {
    field: string;
    value: string;
}

export interface SearchTermInput {
    fields: Nullable<string>[];
    value: string;
    match?: Nullable<Match>;
}

export interface FindRequest {
    first?: Nullable<number>;
    after?: Nullable<string>;
    last?: Nullable<number>;
    before?: Nullable<string>;
    filters?: Nullable<Nullable<FilterSetting>[]>;
    searchTerms?: Nullable<SearchTerm>;
}

export interface FindSettings {
    searchableFields: Nullable<string>[];
    filters: Nullable<FilterSetting>[];
}

export interface FilterSetting {
    field: string;
    text: string;
    options: Nullable<FilterOption>[];
    multi: boolean;
}

export interface FilterOption {
    value: string;
    text: string;
}

export interface SearchTerm {
    fields?: Nullable<Nullable<string>[]>;
    value?: Nullable<string>;
    match?: Nullable<Match>;
}

export interface PageInfo {
    endCursor?: Nullable<string>;
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    startCursor?: Nullable<string>;
}

export interface IQuery {
    findSettings(): FindSettings | Promise<FindSettings>;
}

type Nullable<T> = T | null;
