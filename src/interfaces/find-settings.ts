/* eslint-disable */
import * as _m0 from "protobufjs/minimal";

export const protobufPackage = "find_settings";

export interface FindSettings {
  searchableFields: string[];
  filters: FindSettings_FilterSetting[];
  orders: FindSettings_OrderSetting[];
}

export interface FindSettings_FilterSetting {
  field: string;
  text: string;
  options: FindSettings_FilterSetting_FilterOption[];
  multi: boolean;
}

export interface FindSettings_FilterSetting_FilterOption {
  value: string;
  text: string;
}

export interface FindSettings_OrderSetting {
  field: string;
  text: string;
}

export interface EmptyRequest {
}

export interface FindFilter {
  field: string;
  value: string;
}

export interface SearchTerm {
  fields: string[];
  value: string;
  match: SearchTerm_Match;
}

export enum SearchTerm_Match {
  LIKE = 0,
  EXACT = 1,
  UNRECOGNIZED = -1,
}

export function searchTerm_MatchFromJSON(object: any): SearchTerm_Match {
  switch (object) {
    case 0:
    case "LIKE":
      return SearchTerm_Match.LIKE;
    case 1:
    case "EXACT":
      return SearchTerm_Match.EXACT;
    case -1:
    case "UNRECOGNIZED":
    default:
      return SearchTerm_Match.UNRECOGNIZED;
  }
}

export function searchTerm_MatchToJSON(object: SearchTerm_Match): string {
  switch (object) {
    case SearchTerm_Match.LIKE:
      return "LIKE";
    case SearchTerm_Match.EXACT:
      return "EXACT";
    case SearchTerm_Match.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface PageInfo {
  endCursor: string;
  hasNextPage: boolean;
  startCursor: string;
  hasPreviousPage: boolean;
}

function createBaseFindSettings(): FindSettings {
  return { searchableFields: [], filters: [], orders: [] };
}

export const FindSettings = {
  encode(message: FindSettings, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.searchableFields) {
      writer.uint32(10).string(v!);
    }
    for (const v of message.filters) {
      FindSettings_FilterSetting.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.orders) {
      FindSettings_OrderSetting.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindSettings {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindSettings();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.searchableFields.push(reader.string());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.filters.push(FindSettings_FilterSetting.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.orders.push(FindSettings_OrderSetting.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindSettings {
    return {
      searchableFields: globalThis.Array.isArray(object?.searchableFields)
        ? object.searchableFields.map((e: any) => globalThis.String(e))
        : [],
      filters: globalThis.Array.isArray(object?.filters)
        ? object.filters.map((e: any) => FindSettings_FilterSetting.fromJSON(e))
        : [],
      orders: globalThis.Array.isArray(object?.orders)
        ? object.orders.map((e: any) => FindSettings_OrderSetting.fromJSON(e))
        : [],
    };
  },

  toJSON(message: FindSettings): unknown {
    const obj: any = {};
    if (message.searchableFields?.length) {
      obj.searchableFields = message.searchableFields;
    }
    if (message.filters?.length) {
      obj.filters = message.filters.map((e) => FindSettings_FilterSetting.toJSON(e));
    }
    if (message.orders?.length) {
      obj.orders = message.orders.map((e) => FindSettings_OrderSetting.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindSettings>, I>>(base?: I): FindSettings {
    return FindSettings.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindSettings>, I>>(object: I): FindSettings {
    const message = createBaseFindSettings();
    message.searchableFields = object.searchableFields?.map((e) => e) || [];
    message.filters = object.filters?.map((e) => FindSettings_FilterSetting.fromPartial(e)) || [];
    message.orders = object.orders?.map((e) => FindSettings_OrderSetting.fromPartial(e)) || [];
    return message;
  },
};

function createBaseFindSettings_FilterSetting(): FindSettings_FilterSetting {
  return { field: "", text: "", options: [], multi: false };
}

export const FindSettings_FilterSetting = {
  encode(message: FindSettings_FilterSetting, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.field !== "") {
      writer.uint32(10).string(message.field);
    }
    if (message.text !== "") {
      writer.uint32(18).string(message.text);
    }
    for (const v of message.options) {
      FindSettings_FilterSetting_FilterOption.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.multi === true) {
      writer.uint32(32).bool(message.multi);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindSettings_FilterSetting {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindSettings_FilterSetting();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.field = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.text = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.options.push(FindSettings_FilterSetting_FilterOption.decode(reader, reader.uint32()));
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.multi = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindSettings_FilterSetting {
    return {
      field: isSet(object.field) ? globalThis.String(object.field) : "",
      text: isSet(object.text) ? globalThis.String(object.text) : "",
      options: globalThis.Array.isArray(object?.options)
        ? object.options.map((e: any) => FindSettings_FilterSetting_FilterOption.fromJSON(e))
        : [],
      multi: isSet(object.multi) ? globalThis.Boolean(object.multi) : false,
    };
  },

  toJSON(message: FindSettings_FilterSetting): unknown {
    const obj: any = {};
    if (message.field !== "") {
      obj.field = message.field;
    }
    if (message.text !== "") {
      obj.text = message.text;
    }
    if (message.options?.length) {
      obj.options = message.options.map((e) => FindSettings_FilterSetting_FilterOption.toJSON(e));
    }
    if (message.multi === true) {
      obj.multi = message.multi;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindSettings_FilterSetting>, I>>(base?: I): FindSettings_FilterSetting {
    return FindSettings_FilterSetting.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindSettings_FilterSetting>, I>>(object: I): FindSettings_FilterSetting {
    const message = createBaseFindSettings_FilterSetting();
    message.field = object.field ?? "";
    message.text = object.text ?? "";
    message.options = object.options?.map((e) => FindSettings_FilterSetting_FilterOption.fromPartial(e)) || [];
    message.multi = object.multi ?? false;
    return message;
  },
};

function createBaseFindSettings_FilterSetting_FilterOption(): FindSettings_FilterSetting_FilterOption {
  return { value: "", text: "" };
}

export const FindSettings_FilterSetting_FilterOption = {
  encode(message: FindSettings_FilterSetting_FilterOption, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.value !== "") {
      writer.uint32(10).string(message.value);
    }
    if (message.text !== "") {
      writer.uint32(18).string(message.text);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindSettings_FilterSetting_FilterOption {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindSettings_FilterSetting_FilterOption();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.value = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.text = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindSettings_FilterSetting_FilterOption {
    return {
      value: isSet(object.value) ? globalThis.String(object.value) : "",
      text: isSet(object.text) ? globalThis.String(object.text) : "",
    };
  },

  toJSON(message: FindSettings_FilterSetting_FilterOption): unknown {
    const obj: any = {};
    if (message.value !== "") {
      obj.value = message.value;
    }
    if (message.text !== "") {
      obj.text = message.text;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindSettings_FilterSetting_FilterOption>, I>>(
    base?: I,
  ): FindSettings_FilterSetting_FilterOption {
    return FindSettings_FilterSetting_FilterOption.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindSettings_FilterSetting_FilterOption>, I>>(
    object: I,
  ): FindSettings_FilterSetting_FilterOption {
    const message = createBaseFindSettings_FilterSetting_FilterOption();
    message.value = object.value ?? "";
    message.text = object.text ?? "";
    return message;
  },
};

function createBaseFindSettings_OrderSetting(): FindSettings_OrderSetting {
  return { field: "", text: "" };
}

export const FindSettings_OrderSetting = {
  encode(message: FindSettings_OrderSetting, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.field !== "") {
      writer.uint32(10).string(message.field);
    }
    if (message.text !== "") {
      writer.uint32(18).string(message.text);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindSettings_OrderSetting {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindSettings_OrderSetting();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.field = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.text = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindSettings_OrderSetting {
    return {
      field: isSet(object.field) ? globalThis.String(object.field) : "",
      text: isSet(object.text) ? globalThis.String(object.text) : "",
    };
  },

  toJSON(message: FindSettings_OrderSetting): unknown {
    const obj: any = {};
    if (message.field !== "") {
      obj.field = message.field;
    }
    if (message.text !== "") {
      obj.text = message.text;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindSettings_OrderSetting>, I>>(base?: I): FindSettings_OrderSetting {
    return FindSettings_OrderSetting.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindSettings_OrderSetting>, I>>(object: I): FindSettings_OrderSetting {
    const message = createBaseFindSettings_OrderSetting();
    message.field = object.field ?? "";
    message.text = object.text ?? "";
    return message;
  },
};

function createBaseEmptyRequest(): EmptyRequest {
  return {};
}

export const EmptyRequest = {
  encode(_: EmptyRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EmptyRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEmptyRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): EmptyRequest {
    return {};
  },

  toJSON(_: EmptyRequest): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<EmptyRequest>, I>>(base?: I): EmptyRequest {
    return EmptyRequest.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<EmptyRequest>, I>>(_: I): EmptyRequest {
    const message = createBaseEmptyRequest();
    return message;
  },
};

function createBaseFindFilter(): FindFilter {
  return { field: "", value: "" };
}

export const FindFilter = {
  encode(message: FindFilter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.field !== "") {
      writer.uint32(10).string(message.field);
    }
    if (message.value !== "") {
      writer.uint32(18).string(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindFilter {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindFilter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.field = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.value = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindFilter {
    return {
      field: isSet(object.field) ? globalThis.String(object.field) : "",
      value: isSet(object.value) ? globalThis.String(object.value) : "",
    };
  },

  toJSON(message: FindFilter): unknown {
    const obj: any = {};
    if (message.field !== "") {
      obj.field = message.field;
    }
    if (message.value !== "") {
      obj.value = message.value;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindFilter>, I>>(base?: I): FindFilter {
    return FindFilter.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindFilter>, I>>(object: I): FindFilter {
    const message = createBaseFindFilter();
    message.field = object.field ?? "";
    message.value = object.value ?? "";
    return message;
  },
};

function createBaseSearchTerm(): SearchTerm {
  return { fields: [], value: "", match: 0 };
}

export const SearchTerm = {
  encode(message: SearchTerm, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.fields) {
      writer.uint32(10).string(v!);
    }
    if (message.value !== "") {
      writer.uint32(18).string(message.value);
    }
    if (message.match !== 0) {
      writer.uint32(24).int32(message.match);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SearchTerm {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSearchTerm();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.fields.push(reader.string());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.value = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.match = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SearchTerm {
    return {
      fields: globalThis.Array.isArray(object?.fields) ? object.fields.map((e: any) => globalThis.String(e)) : [],
      value: isSet(object.value) ? globalThis.String(object.value) : "",
      match: isSet(object.match) ? searchTerm_MatchFromJSON(object.match) : 0,
    };
  },

  toJSON(message: SearchTerm): unknown {
    const obj: any = {};
    if (message.fields?.length) {
      obj.fields = message.fields;
    }
    if (message.value !== "") {
      obj.value = message.value;
    }
    if (message.match !== 0) {
      obj.match = searchTerm_MatchToJSON(message.match);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SearchTerm>, I>>(base?: I): SearchTerm {
    return SearchTerm.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<SearchTerm>, I>>(object: I): SearchTerm {
    const message = createBaseSearchTerm();
    message.fields = object.fields?.map((e) => e) || [];
    message.value = object.value ?? "";
    message.match = object.match ?? 0;
    return message;
  },
};

function createBasePageInfo(): PageInfo {
  return { endCursor: "", hasNextPage: false, startCursor: "", hasPreviousPage: false };
}

export const PageInfo = {
  encode(message: PageInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.endCursor !== "") {
      writer.uint32(10).string(message.endCursor);
    }
    if (message.hasNextPage === true) {
      writer.uint32(16).bool(message.hasNextPage);
    }
    if (message.startCursor !== "") {
      writer.uint32(26).string(message.startCursor);
    }
    if (message.hasPreviousPage === true) {
      writer.uint32(32).bool(message.hasPreviousPage);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PageInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePageInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.endCursor = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.hasNextPage = reader.bool();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.startCursor = reader.string();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.hasPreviousPage = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): PageInfo {
    return {
      endCursor: isSet(object.endCursor) ? globalThis.String(object.endCursor) : "",
      hasNextPage: isSet(object.hasNextPage) ? globalThis.Boolean(object.hasNextPage) : false,
      startCursor: isSet(object.startCursor) ? globalThis.String(object.startCursor) : "",
      hasPreviousPage: isSet(object.hasPreviousPage) ? globalThis.Boolean(object.hasPreviousPage) : false,
    };
  },

  toJSON(message: PageInfo): unknown {
    const obj: any = {};
    if (message.endCursor !== "") {
      obj.endCursor = message.endCursor;
    }
    if (message.hasNextPage === true) {
      obj.hasNextPage = message.hasNextPage;
    }
    if (message.startCursor !== "") {
      obj.startCursor = message.startCursor;
    }
    if (message.hasPreviousPage === true) {
      obj.hasPreviousPage = message.hasPreviousPage;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<PageInfo>, I>>(base?: I): PageInfo {
    return PageInfo.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<PageInfo>, I>>(object: I): PageInfo {
    const message = createBasePageInfo();
    message.endCursor = object.endCursor ?? "";
    message.hasNextPage = object.hasNextPage ?? false;
    message.startCursor = object.startCursor ?? "";
    message.hasPreviousPage = object.hasPreviousPage ?? false;
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends globalThis.Array<infer U> ? globalThis.Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
