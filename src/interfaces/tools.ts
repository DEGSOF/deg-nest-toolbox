/* eslint-disable */
import { Metadata } from "@grpc/grpc-js";
import * as _m0 from "protobufjs/minimal";
import { FindFilter, FindSettings, PageInfo, SearchTerm } from "./find-settings";

export const protobufPackage = "tools";

export interface EmptyRequest {
}

export interface EmptyResponse {
}

export interface FindRequest {
  first: number;
  after: string;
  last: number;
  before: string;
  filters: FindFilter[];
  order: string;
  searchTerms: SearchTerm[];
}

export interface SetToolRequest {
  id: string;
  name: string;
}

export interface FindToolsResponse {
  pageInfo: PageInfo | undefined;
  totalCount: number;
  edges: ToolsEdge[];
}

export interface ToolsEdge {
  cursor: string;
  node: Tool | undefined;
}

export interface Tool {
  id: string;
  name: string;
}

export interface SuccessDeletedResponse {
  success: boolean;
  tool: Tool | undefined;
}

export interface DeleteElementRequest {
  id: string;
}

function createBaseEmptyRequest(): EmptyRequest {
  return {};
}

export const EmptyRequest = {
  encode(_: EmptyRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EmptyRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEmptyRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): EmptyRequest {
    return {};
  },

  toJSON(_: EmptyRequest): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<EmptyRequest>, I>>(base?: I): EmptyRequest {
    return EmptyRequest.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<EmptyRequest>, I>>(_: I): EmptyRequest {
    const message = createBaseEmptyRequest();
    return message;
  },
};

function createBaseEmptyResponse(): EmptyResponse {
  return {};
}

export const EmptyResponse = {
  encode(_: EmptyResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EmptyResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEmptyResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): EmptyResponse {
    return {};
  },

  toJSON(_: EmptyResponse): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<EmptyResponse>, I>>(base?: I): EmptyResponse {
    return EmptyResponse.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<EmptyResponse>, I>>(_: I): EmptyResponse {
    const message = createBaseEmptyResponse();
    return message;
  },
};

function createBaseFindRequest(): FindRequest {
  return { first: 0, after: "", last: 0, before: "", filters: [], order: "", searchTerms: [] };
}

export const FindRequest = {
  encode(message: FindRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.first !== 0) {
      writer.uint32(8).uint32(message.first);
    }
    if (message.after !== "") {
      writer.uint32(18).string(message.after);
    }
    if (message.last !== 0) {
      writer.uint32(24).uint32(message.last);
    }
    if (message.before !== "") {
      writer.uint32(34).string(message.before);
    }
    for (const v of message.filters) {
      FindFilter.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    if (message.order !== "") {
      writer.uint32(50).string(message.order);
    }
    for (const v of message.searchTerms) {
      SearchTerm.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.first = reader.uint32();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.after = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.last = reader.uint32();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.before = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.filters.push(FindFilter.decode(reader, reader.uint32()));
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.order = reader.string();
          continue;
        case 7:
          if (tag !== 58) {
            break;
          }

          message.searchTerms.push(SearchTerm.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindRequest {
    return {
      first: isSet(object.first) ? globalThis.Number(object.first) : 0,
      after: isSet(object.after) ? globalThis.String(object.after) : "",
      last: isSet(object.last) ? globalThis.Number(object.last) : 0,
      before: isSet(object.before) ? globalThis.String(object.before) : "",
      filters: globalThis.Array.isArray(object?.filters) ? object.filters.map((e: any) => FindFilter.fromJSON(e)) : [],
      order: isSet(object.order) ? globalThis.String(object.order) : "",
      searchTerms: globalThis.Array.isArray(object?.searchTerms)
        ? object.searchTerms.map((e: any) => SearchTerm.fromJSON(e))
        : [],
    };
  },

  toJSON(message: FindRequest): unknown {
    const obj: any = {};
    if (message.first !== 0) {
      obj.first = Math.round(message.first);
    }
    if (message.after !== "") {
      obj.after = message.after;
    }
    if (message.last !== 0) {
      obj.last = Math.round(message.last);
    }
    if (message.before !== "") {
      obj.before = message.before;
    }
    if (message.filters?.length) {
      obj.filters = message.filters.map((e) => FindFilter.toJSON(e));
    }
    if (message.order !== "") {
      obj.order = message.order;
    }
    if (message.searchTerms?.length) {
      obj.searchTerms = message.searchTerms.map((e) => SearchTerm.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindRequest>, I>>(base?: I): FindRequest {
    return FindRequest.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindRequest>, I>>(object: I): FindRequest {
    const message = createBaseFindRequest();
    message.first = object.first ?? 0;
    message.after = object.after ?? "";
    message.last = object.last ?? 0;
    message.before = object.before ?? "";
    message.filters = object.filters?.map((e) => FindFilter.fromPartial(e)) || [];
    message.order = object.order ?? "";
    message.searchTerms = object.searchTerms?.map((e) => SearchTerm.fromPartial(e)) || [];
    return message;
  },
};

function createBaseSetToolRequest(): SetToolRequest {
  return { id: "", name: "" };
}

export const SetToolRequest = {
  encode(message: SetToolRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SetToolRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSetToolRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.id = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.name = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SetToolRequest {
    return {
      id: isSet(object.id) ? globalThis.String(object.id) : "",
      name: isSet(object.name) ? globalThis.String(object.name) : "",
    };
  },

  toJSON(message: SetToolRequest): unknown {
    const obj: any = {};
    if (message.id !== "") {
      obj.id = message.id;
    }
    if (message.name !== "") {
      obj.name = message.name;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SetToolRequest>, I>>(base?: I): SetToolRequest {
    return SetToolRequest.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<SetToolRequest>, I>>(object: I): SetToolRequest {
    const message = createBaseSetToolRequest();
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseFindToolsResponse(): FindToolsResponse {
  return { pageInfo: undefined, totalCount: 0, edges: [] };
}

export const FindToolsResponse = {
  encode(message: FindToolsResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.pageInfo !== undefined) {
      PageInfo.encode(message.pageInfo, writer.uint32(10).fork()).ldelim();
    }
    if (message.totalCount !== 0) {
      writer.uint32(16).uint32(message.totalCount);
    }
    for (const v of message.edges) {
      ToolsEdge.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FindToolsResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFindToolsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.pageInfo = PageInfo.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.totalCount = reader.uint32();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.edges.push(ToolsEdge.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FindToolsResponse {
    return {
      pageInfo: isSet(object.pageInfo) ? PageInfo.fromJSON(object.pageInfo) : undefined,
      totalCount: isSet(object.totalCount) ? globalThis.Number(object.totalCount) : 0,
      edges: globalThis.Array.isArray(object?.edges) ? object.edges.map((e: any) => ToolsEdge.fromJSON(e)) : [],
    };
  },

  toJSON(message: FindToolsResponse): unknown {
    const obj: any = {};
    if (message.pageInfo !== undefined) {
      obj.pageInfo = PageInfo.toJSON(message.pageInfo);
    }
    if (message.totalCount !== 0) {
      obj.totalCount = Math.round(message.totalCount);
    }
    if (message.edges?.length) {
      obj.edges = message.edges.map((e) => ToolsEdge.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FindToolsResponse>, I>>(base?: I): FindToolsResponse {
    return FindToolsResponse.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<FindToolsResponse>, I>>(object: I): FindToolsResponse {
    const message = createBaseFindToolsResponse();
    message.pageInfo = (object.pageInfo !== undefined && object.pageInfo !== null)
      ? PageInfo.fromPartial(object.pageInfo)
      : undefined;
    message.totalCount = object.totalCount ?? 0;
    message.edges = object.edges?.map((e) => ToolsEdge.fromPartial(e)) || [];
    return message;
  },
};

function createBaseToolsEdge(): ToolsEdge {
  return { cursor: "", node: undefined };
}

export const ToolsEdge = {
  encode(message: ToolsEdge, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.cursor !== "") {
      writer.uint32(10).string(message.cursor);
    }
    if (message.node !== undefined) {
      Tool.encode(message.node, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ToolsEdge {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseToolsEdge();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.cursor = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.node = Tool.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ToolsEdge {
    return {
      cursor: isSet(object.cursor) ? globalThis.String(object.cursor) : "",
      node: isSet(object.node) ? Tool.fromJSON(object.node) : undefined,
    };
  },

  toJSON(message: ToolsEdge): unknown {
    const obj: any = {};
    if (message.cursor !== "") {
      obj.cursor = message.cursor;
    }
    if (message.node !== undefined) {
      obj.node = Tool.toJSON(message.node);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ToolsEdge>, I>>(base?: I): ToolsEdge {
    return ToolsEdge.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<ToolsEdge>, I>>(object: I): ToolsEdge {
    const message = createBaseToolsEdge();
    message.cursor = object.cursor ?? "";
    message.node = (object.node !== undefined && object.node !== null) ? Tool.fromPartial(object.node) : undefined;
    return message;
  },
};

function createBaseTool(): Tool {
  return { id: "", name: "" };
}

export const Tool = {
  encode(message: Tool, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Tool {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTool();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.id = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.name = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): Tool {
    return {
      id: isSet(object.id) ? globalThis.String(object.id) : "",
      name: isSet(object.name) ? globalThis.String(object.name) : "",
    };
  },

  toJSON(message: Tool): unknown {
    const obj: any = {};
    if (message.id !== "") {
      obj.id = message.id;
    }
    if (message.name !== "") {
      obj.name = message.name;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<Tool>, I>>(base?: I): Tool {
    return Tool.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<Tool>, I>>(object: I): Tool {
    const message = createBaseTool();
    message.id = object.id ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

function createBaseSuccessDeletedResponse(): SuccessDeletedResponse {
  return { success: false, tool: undefined };
}

export const SuccessDeletedResponse = {
  encode(message: SuccessDeletedResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    if (message.tool !== undefined) {
      Tool.encode(message.tool, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SuccessDeletedResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSuccessDeletedResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.tool = Tool.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SuccessDeletedResponse {
    return {
      success: isSet(object.success) ? globalThis.Boolean(object.success) : false,
      tool: isSet(object.tool) ? Tool.fromJSON(object.tool) : undefined,
    };
  },

  toJSON(message: SuccessDeletedResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    if (message.tool !== undefined) {
      obj.tool = Tool.toJSON(message.tool);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SuccessDeletedResponse>, I>>(base?: I): SuccessDeletedResponse {
    return SuccessDeletedResponse.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<SuccessDeletedResponse>, I>>(object: I): SuccessDeletedResponse {
    const message = createBaseSuccessDeletedResponse();
    message.success = object.success ?? false;
    message.tool = (object.tool !== undefined && object.tool !== null) ? Tool.fromPartial(object.tool) : undefined;
    return message;
  },
};

function createBaseDeleteElementRequest(): DeleteElementRequest {
  return { id: "" };
}

export const DeleteElementRequest = {
  encode(message: DeleteElementRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteElementRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDeleteElementRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.id = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DeleteElementRequest {
    return { id: isSet(object.id) ? globalThis.String(object.id) : "" };
  },

  toJSON(message: DeleteElementRequest): unknown {
    const obj: any = {};
    if (message.id !== "") {
      obj.id = message.id;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DeleteElementRequest>, I>>(base?: I): DeleteElementRequest {
    return DeleteElementRequest.fromPartial(base ?? ({} as any));
  },
  fromPartial<I extends Exact<DeepPartial<DeleteElementRequest>, I>>(object: I): DeleteElementRequest {
    const message = createBaseDeleteElementRequest();
    message.id = object.id ?? "";
    return message;
  },
};

export interface Tools {
  FindToolSettings(request: EmptyRequest, metadata?: Metadata): Promise<FindSettings>;
  FindTool(request: FindRequest, metadata?: Metadata): Promise<FindToolsResponse>;
  SetTool(request: SetToolRequest, metadata?: Metadata): Promise<Tool>;
  SoftDeleteTool(request: DeleteElementRequest, metadata?: Metadata): Promise<SuccessDeletedResponse>;
  HardDeleteTool(request: DeleteElementRequest, metadata?: Metadata): Promise<SuccessDeletedResponse>;
}

export const ToolsServiceName = "tools.Tools";
export class ToolsClientImpl implements Tools {
  private readonly rpc: Rpc;
  private readonly service: string;
  constructor(rpc: Rpc, opts?: { service?: string }) {
    this.service = opts?.service || ToolsServiceName;
    this.rpc = rpc;
    this.FindToolSettings = this.FindToolSettings.bind(this);
    this.FindTool = this.FindTool.bind(this);
    this.SetTool = this.SetTool.bind(this);
    this.SoftDeleteTool = this.SoftDeleteTool.bind(this);
    this.HardDeleteTool = this.HardDeleteTool.bind(this);
  }
  FindToolSettings(request: EmptyRequest): Promise<FindSettings> {
    const data = EmptyRequest.encode(request).finish();
    const promise = this.rpc.request(this.service, "FindToolSettings", data);
    return promise.then((data) => FindSettings.decode(_m0.Reader.create(data)));
  }

  FindTool(request: FindRequest): Promise<FindToolsResponse> {
    const data = FindRequest.encode(request).finish();
    const promise = this.rpc.request(this.service, "FindTool", data);
    return promise.then((data) => FindToolsResponse.decode(_m0.Reader.create(data)));
  }

  SetTool(request: SetToolRequest): Promise<Tool> {
    const data = SetToolRequest.encode(request).finish();
    const promise = this.rpc.request(this.service, "SetTool", data);
    return promise.then((data) => Tool.decode(_m0.Reader.create(data)));
  }

  SoftDeleteTool(request: DeleteElementRequest): Promise<SuccessDeletedResponse> {
    const data = DeleteElementRequest.encode(request).finish();
    const promise = this.rpc.request(this.service, "SoftDeleteTool", data);
    return promise.then((data) => SuccessDeletedResponse.decode(_m0.Reader.create(data)));
  }

  HardDeleteTool(request: DeleteElementRequest): Promise<SuccessDeletedResponse> {
    const data = DeleteElementRequest.encode(request).finish();
    const promise = this.rpc.request(this.service, "HardDeleteTool", data);
    return promise.then((data) => SuccessDeletedResponse.decode(_m0.Reader.create(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends globalThis.Array<infer U> ? globalThis.Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
