### Migraciones
Agregar estos scripts para permitir y manejar las migraciones de base de datos
```
"migrate": "npm run typeorm:migrate",
"typeorm": "typeorm-ts-node-esm",
"typeorm:migrate": "npm run typeorm migration:run -- -d ./src/database/typeorm.config.ts",
"typeorm:generate": "npm run typeorm -- -d ./src/database/typeorm.config.ts migration:generate ./src/database/migrations/%npm_config_name%"
```

### Dependencias
- [x] "@grpc/grpc-js": "^1.9.9",
- [x] "@nestjs/common": "^10.0.0",
- [x] "@nestjs/config": "^3.1.1",
- [x] "@nestjs/core": "^10.0.0",
- [x] "@nestjs/graphql": "^12.0.9",
- [x] "@nestjs/jwt": "^10.1.1",
- [x] "@nestjs/microservices": "^10.2.8",
- [x] "@nestjs/platform-express": "^10.0.0",
- [x] "@nestjs/swagger": "^7.1.14",
- [x] "@nestjs/typeorm": "^10.0.0",
- [x] "class-transformer": "^0.5.1",
- [x] "class-validator": "^0.14.0",
- [x] "helmet": "^7.0.0",
- [x] "moment": "^2.29.4",
- [x] "mysql2": "^3.6.3",
- [x] "reflect-metadata": "^0.1.13",
- [x] "rxjs": "^7.8.1",
- [x] "typeorm": "^0.3.17",
- [x] "ulid": "^2.3.0"

### Modo de uso
1. Crear proyecto nestjs, con el cli normalmente.
2. Instalar Dependencias
3. Crear un recurso con el comando <b>nest g resource </b> (En este caso, a modo de ejemplo, Users)
4. Eliminar todo el contenido del controller y del service dejandolo de la siguiente manera

<b>Controller: </b>
```
import { Controller } from '@nestjs/common';

import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
}
```
<b>Service: </b>
```
import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {}

```
5. Extender la entidad creada (user en este caso) a BaseEntity (O la que se necesite de las que disponibilizamos y dejamos explicadas mas adelante)

```
import { BaseEntity } from 'typeorm';
@Entity('users')
export class User extends BaseEntity {}
````
6. Extender el controller de la siguiente manera
```
import { Controller } from '@nestjs/common';
import { controllerFactory } from 'src/generics/factories/ControllerFactory.factory';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController extends controllerFactory<User, CreateUserDto, UpdateUserDto>(User, CreateUserDto, UpdateUserDto) {
  constructor(private readonly usersService: UsersService) {
    super(usersService);
  }
}

```
7. Crear carpeta repository dentro de la entity en la que se este trabajando y dentro de esa carpeta el archivo {entity}.repository.ts de la siguiente manera
```
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GenericRepository } from 'src/generics/GenericRepository.repository';

import { User } from '../entities/user.entity';

@Injectable()
export class UserRepository extends GenericRepository<User> {
  constructor(
    @InjectRepository(User)
    private repo: GenericRepository<User>,
  ) {
    super(repo.target, repo.manager, repo.queryRunner);
  }
}

```
8. En el {entity}.module.ts donde se este trabajando, incluir el import de typeormModule, con la entidad correspondiente y exportarlo, incluir el repository como un provider tambien.
```
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './entities/user.entity';
import { UserRepository } from './repository/users.repository';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService, UserRepository],
  imports: [TypeOrmModule.forFeature([User])],
  exports: [TypeOrmModule],
})
export class UsersModule {}
```

9. En app.module.ts incluir un constructor que reciba el datasource, incluir el import de databaseModule, configModule y cualquier otro modulo necesario.

```
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DataSource } from 'typeorm';

import { DatabaseModule } from './database/database.module';
import { validate } from './env.validation';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule.forRoot({
      isGlobal: true,
      validate,
    }),
    UsersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
```

10. El archivo env.valdiation.ts contiene las claves que deben ser validas en el archivo .env configurar como sea necesario
```
import { Expose, plainToClass } from 'class-transformer';
import { IsBoolean, IsNumber, IsString, validateSync } from 'class-validator';

import { ToBoolean } from './generics/transformers/toBoolean';

export enum Environment {
  DEVELOP,
  PRODUCTION,
  TEST,
}

export class EnvironmentVariables {
  @IsString()
  @Expose()
  NODE_ENV: string;
  @IsString()
  @Expose()
  TYPEORM_CONNECTION: string;
  @IsString()
  @Expose()
  TYPEORM_HOST: string;
  @IsNumber()
  @Expose()
  TYPEORM_PORT: number;
  @IsNumber()
  @Expose()
  PORT: number;
  @IsBoolean()
  @Expose()
  @ToBoolean()
  NEED_SQL_LOG: boolean;
  @IsString()
  @Expose()
  TYPEORM_USERNAME: string;
  @IsString()
  @Expose()
  TYPEORM_PASSWORD: string;
  @IsString()
  @Expose()
  TYPEORM_DATABASE: string;
  @IsBoolean()
  @Expose()
  @ToBoolean()
  TYPEORM_AUTO_SCHEMA_SYNC: boolean;
  @IsString()
  @Expose()
  TYPEORM_MIGRATIONS_TABLE_NAME: string;
  @IsString()
  @Expose()
  TYPEORM_ENTITY_PREFIX: string;
}

export function validate(config: Record<string, unknown>): EnvironmentVariables {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    excludeExtraneousValues: true,
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
    forbidNonWhitelisted: true,
  });
  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}

```

11. Configurar el archivo main segun necesidades:
```
import 'reflect-metadata';

import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import * as helmet from 'helmet';

import { AppModule } from './app.module';
import { HttpExceptionFilter } from './generics/filters/deg-exceptions-handler.filter';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api'); //prefijo de los endpoints REST
  app.use( // Configuracion de seguridad
    helmet.contentSecurityPolicy({
      directives: {
        ...helmet.contentSecurityPolicy.getDefaultDirectives(),
        'img-src': ["'self'", "'cdn.jsdelivr.net'"],
        'script-src': ["'self'", 'cdn.jsdelivr.net', "'unsafe-inline'"],
      },
    }),
  );
  useContainer(app.select(AppModule), {
    fallbackOnErrors: true,
  });
  app.useGlobalFilters(new HttpExceptionFilter()); // Filtro de excepciones
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors) => new BadRequestException(errors),
      whitelist: true,
      transform: true,
      forbidNonWhitelisted: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );

  const config = new DocumentBuilder() //Configuracion de swagger
    .addApiKey(
      {
        type: 'apiKey',
        in: 'header',
        name: 'x-api-key',
      },
      'x-api-key',
    )
    .addSecurityRequirements('x-api-key')
    .setTitle('NodeBackend Generic')
    .setDescription('The API description')
    .setVersion('0.0.1')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.enableCors();
  const configService = app.get(ConfigService);
  const port = configService.get('PORT');
  await app.listen(port);
  console.log(`Server running on port ${port}`);
}
bootstrap();
//npm run typeorm:generate-migration -name=init

```

12. Correr migraciónes de ser necesario.